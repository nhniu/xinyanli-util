

import lombok.Data;
import lombok.experimental.Accessors;
import java.util.Date;


/**
 * ${tableComment}表
 * ${tableName}
 *
 */
@Data
@Accessors(chain = true)
public class ${javaClassName} {

    public static final String TNAME = "${tableName}";

    <#list items as p>
	/**
	 * ${p.colComment}
	 */
	private ${p.javaType} ${p.javaName};

   </#list>
}