package cn.xinyanli.config;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.CharUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.SneakyThrows;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Getter
public class PropUtil {

    Properties properties;

    /**
     * 加载本地配置文件
     *
     * @param propFile
     */
    @SneakyThrows
    public static PropUtil loadProp(String propFile) {
        Assert.notEmpty(propFile);
        PropUtil util = new PropUtil();
        Properties properties = new Properties();
        properties.load(EnvConfig.class.getClassLoader().getResourceAsStream(propFile));
        util.properties = properties;
        return util;
    }

    @SneakyThrows
    public static Properties loadProp0(String propFile) {
        Assert.notEmpty(propFile);
        Properties properties = new Properties();
        InputStream resourceAsStream = EnvConfig.class.getClassLoader().getResourceAsStream(propFile);
        Assert.notNull(resourceAsStream, propFile + " not exist");
        properties.load(resourceAsStream);
        return properties;
    }

    public <T> T subProp(String prefix, Class<T> config) {
        return subProp(this.properties, prefix, config);
    }
    public <T> T subProp(Class<T> config) {
        return subProp(this.properties, "", config);
    }

    /**
     * 筛选properties
     *
     * @param prefix 以这个开头的将会筛选出来 并且去掉这个标识
     */
    public static Map<String, Object> subProp(Properties properties, String prefix) {
        prefix = prefix + ".";
        Map<String, Object> map = new HashMap<>();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            String key = entry.getKey().toString();
            if (key.startsWith(prefix)) {
                String key1 = StrUtil.subAfter(key, prefix, false);
                // 点转驼峰
                map.put(pointToCamelCase(key1), entry.getValue());
            }
        }
        return map;
    }

    public static <T> T subProp(Properties properties, String prefix, Class<T> config) {
        if (StrUtil.isEmpty(prefix)) {
            return BeanUtil.mapToBean(properties, config, CopyOptions.create().ignoreNullValue());
        } else {
            Map<String, Object> map = subProp(properties, prefix);
            return BeanUtil.mapToBean(map, config, CopyOptions.create().ignoreNullValue());
        }
    }


    /**
     * 点转驼峰
     *
     * @param name
     * @return
     */
    public static String pointToCamelCase(CharSequence name) {
        if (null == name) {
            return null;
        }

        String name2 = name.toString();
        if (name2.contains(".")) {
            final StringBuilder sb = new StringBuilder(name2.length());
            boolean upperCase = false;
            for (int i = 0; i < name2.length(); i++) {
                char c = name2.charAt(i);

                if (c == '.') {
                    upperCase = true;
                } else if (upperCase) {
                    sb.append(Character.toUpperCase(c));
                    upperCase = false;
                } else {
                    sb.append(Character.toLowerCase(c));
                }
            }
            return sb.toString();
        } else {
            return name2;
        }
    }
}
