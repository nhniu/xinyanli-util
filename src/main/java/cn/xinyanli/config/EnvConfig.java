package cn.xinyanli.config;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import cn.xinyanli.core.util.ObjUtil;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Properties;

@Slf4j
@Data
public class EnvConfig {
    protected Properties boot;

    protected Properties properties;


    public EnvConfig() {
        this.properties = new Properties();
    }

    public EnvConfig(Properties boot,Properties properties) {
        this.boot = boot;
        this.properties = properties;
    }



    /**
     * 是否是dev环境
     *
     * @return
     */
    public boolean isDev() {
        return getEnv().equals("dev");
    }

    /**
     * 获取当前环境
     *
     * @return
     */
    public String getEnv() {
        String env = boot != null ? boot.getProperty("env") : null;
        log.info("evn={}", env);
        if (StrUtil.isEmpty(env)) {
            env = "dev";
            log.info("set default env = dev");
        }
        return env;
    }


    @SneakyThrows
    public void loadProp(Properties properties) {
        Assert.notEmpty(properties);
        this.properties = properties;
    }

    public String get(String key) {
        return properties.getProperty(key);
    }


    public String get(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public Integer getInt(String key, Integer defaultValue) {
        String value = properties.getProperty(key);
        return StrUtil.isNotEmpty(value) ? Integer.parseInt(value) : defaultValue;
    }

    public Integer getInt(String key) {
        return getInt(key, null);
    }


    public Integer getRequiredInt(String key) {
        return ObjUtil.getRequired(getInt(key, null), key);
    }


    public boolean getBoolean(String key, boolean defaultValue) {
        String value = properties.getProperty(key);
        return StrUtil.isNotEmpty(value) ? Boolean.valueOf(value) : defaultValue;
    }

    /**
     * 筛选properties
     *
     * @param prefix 以这个开头的将会筛选出来 并且去掉这个标识
     */
    public Map<String, Object> subProp(String prefix) {
        return PropUtil.subProp(properties, prefix);
    }

    public <T> T subProp(Class<T> config, String prefix) {
        return PropUtil.subProp(properties, prefix, config);
    }
}
