package cn.xinyanli.db;

import cn.hutool.core.bean.BeanDesc;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.func.Func0;
import cn.hutool.core.lang.func.VoidFunc1;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.xinyanli.config.PropUtil;
import cn.xinyanli.core.util.ColUtil;
import cn.xinyanli.db.annotation.JsonData;
import cn.xinyanli.db.enums.EqType;
import cn.xinyanli.db.lambda.Lq;
import cn.xinyanli.db.pool.DataSourceImpl;
import cn.xinyanli.db.pool.IConnectionPool;
import cn.xinyanli.db.reflect.ReflectUtil0;
import cn.xinyanli.db.sql.SqlUtil;
import com.alibaba.fastjson.JSON;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.*;

import static cn.xinyanli.db.sql.SqlUtil.*;

/**
 * jdbc工具类
 * <p>
 * 配置示例
 * dbpool.url=jdbc:mysql://127.0.0.1:3306/sjmm-mall-dev?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowMultiQueries=true&serverTimezone=Asia/Shanghai
 * dbpool.username=root
 * dbpool.password=root
 * dbpool.driverName=com.mysql.cj.jdbc.Driver
 * db.printSql=false
 *
 * @author hewei
 * @date 2019/2/26 19:58
 **/
@Slf4j
public class JdbcUtil {

    private static final String MARK_QUESTION = "q?";

    private IConnectionPool pool;
    private String databaseName;
    private Builder builder;


    private static void notEmpty(String obj, String filedName) {
        Assert.notEmpty(obj, filedName + " connot be empty");
    }

    @Setter
    @Accessors(chain = true)
    @FieldNameConstants
    public static final class Builder {

        /**
         * 单个连接
         */
        private boolean siginConn;
        private String url;
        private String username;
        private String password;
        private String driver = "com.mysql.cj.jdbc.Driver";

        private int initSize = 2;
        private int maxSize = 4;
        private int stepSize = 1;
        private int timeout = 2000;

        private boolean printDetailSql;
        private boolean printSql;


        public Builder() {
        }

        public Builder propPrifix(String propPath, String prefix) {
            Properties p1 = PropUtil.loadProp0(propPath);
            Map<String, Object> map = PropUtil.subProp(p1, prefix);
            BeanUtil.copyProperties(map, this);

            return this;
        }

        public Builder propMap(Map map) {
            BeanUtil.copyProperties(map, this);
            return this;
        }

        public JdbcUtil build() {
            Assert.notEmpty(url, Fields.url);
            notEmpty(username, Fields.username);
            Assert.notEmpty(password, Fields.password);
            Assert.notEmpty(driver, Fields.driver);
            return new JdbcUtil(this);
        }
    }

    @SneakyThrows
    public JdbcUtil(Builder builder) {
        this.builder = builder;
        // 不用连接池
        if (builder.siginConn) {
            Class.forName(builder.driver);
            Connection connection = DriverManager.getConnection(builder.url, builder.username, builder.password);

            this.pool = new IConnectionPool() {
                @Override
                public Connection getConnection() {
                    return connection;
                }

                @Override
                public void releaseConnection(Connection connection) {
                    // 单个连接不释放
                }
            };
        } else {
            // 使用连接池
            DataSourceImpl.Config poolConfig = new DataSourceImpl.Config()
                    .setDriverName(builder.driver)
                    .setInitSize(builder.initSize)
                    .setMaxSize(builder.maxSize)
                    .setStepSize(builder.stepSize)
                    .setTimeout(builder.timeout)
                    .setUrl(builder.url)
                    .setUsername(builder.username)
                    .setPassword(builder.password);
            this.pool = new DataSourceImpl(poolConfig);
        }
        this.databaseName = String.valueOf(queryOneColumn("select database()"));
    }


    ThreadLocal<Connection> threadConn = new ThreadLocal<>();

    public Connection getConn() {
        // 获取线程变量中有没有连接 ，有是开启了事物
        Connection connection = threadConn.get();
        if (connection != null) {
            return connection;
        } else {
            return pool.getConnection();
        }
    }

    private void close(Connection conn, Collection<PreparedStatement> pss) {
        for (PreparedStatement ps : pss) {
            close(null, ps, null);
        }
        close(null, null, conn);
    }

    /**
     * 释放结果集 ResultSet
     *
     * @param rs
     */
    public void close(ResultSet rs, Statement st, Connection conn) {

        //释放结果集 ResultSet
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.error("", e);
            }
        }

        //释放语句执行者 Statement
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                log.error("", e);
            }
        }

        if (conn != null) {
            // 没有事物才关闭
            if (threadConn.get() == null) {
                pool.releaseConnection(conn);
            }
        }
    }


    /**
     * 批量执行
     */
    @SneakyThrows
    public int[] batchExecute(String sql, VoidFunc1<PreparedStatement> func1) {
        sql = formatMarkSql(sql, sql.split(",").length);

        PreparedStatement ps = null;
        try {
            ps = batchPre(sql);
            func1.call(ps);
            return ps.executeBatch();
        } finally {
            close(null, ps, ps != null ? ps.getConnection() : null);
        }
    }

    /**
     * 批量处理前
     *
     * @param sql
     * @return
     */
    @SneakyThrows
    public PreparedStatement batchPre(String sql) {
        sql = formatMarkSql(sql, sql.split(",").length);
        Connection conn = getConn();
        return getPs(conn, sql, null);
    }

    /**
     * 批量添加 使用PreparedStatement
     * 不要使用Statement自己设置json数据很容易出错
     *
     * @return
     */
    @SneakyThrows
    public void batchAdd(PreparedStatement ps, Object... param) {
        setPsParam(ps, param);
        ps.addBatch();
    }


    /**
     * 批量提交
     *
     * @param ps
     */
    @SneakyThrows
    public int[] batchCommit(PreparedStatement ps) {
        try {
            return ps.executeBatch();
        } finally {
            close(null, ps, ps != null ? ps.getConnection() : null);
        }
    }

    /**
     * 将ResultSet结果集中的记录映射到Map对象中.
     *
     * @param fieldClassName 是JDBC API中的类型名称,
     * @param fieldName      是字段名，
     * @param rs             是一个ResultSet查询结果集,
     * @param fieldValue     Map对象,用于存贮一条记录.
     * @throws SQLException
     */
    private void recordMappingToMap(String fieldClassName, String fieldName, ResultSet rs, Map fieldValue)
            throws SQLException {

        fieldValue.put(fieldName, getFiledVal(fieldClassName, fieldName, rs));
    }


    private Object getFiledVal(String fieldClassName, String fieldName, ResultSet rs)
            throws SQLException {
        // 优先规则：常用类型靠前
        Object s;
        if (fieldClassName.equals("java.lang.String")) {
            s = rs.getString(fieldName);
        } else if (fieldClassName.equals("java.lang.Integer")) {
            s = rs.getInt(fieldName);
        } else if (fieldClassName.equals("java.lang.Long")) {
            s = rs.getLong(fieldName);
        } else if (fieldClassName.equals("java.lang.Boolean")) {
            s = rs.getBoolean(fieldName);
        } else if (fieldClassName.equals("java.lang.Short")) {
            s = rs.getShort(fieldName);
        } else if (fieldClassName.equals("java.lang.Float")) {
            s = rs.getFloat(fieldName);
        } else if (fieldClassName.equals("java.lang.Double")) {
            s = rs.getDouble(fieldName);
        } else if (fieldClassName.equals("java.sql.Timestamp")) {
            s = rs.getTimestamp(fieldName);
        } else if (fieldClassName.equals("java.sql.Date") || fieldClassName.equals("java.util.Date")) {
            s = rs.getDate(fieldName);
        } else if (fieldClassName.equals("java.sql.Time")) {
            s = rs.getTime(fieldName);
        } else if (fieldClassName.equals("java.lang.Byte")) {
            s = rs.getByte(fieldName);
        } else if (fieldClassName.equals("[B") || fieldClassName.equals("byte[]")) {
            // byte[]出现在SQL Server中
            s = rs.getBytes(fieldName);
        } else if (fieldClassName.equals("java.math.BigDecimal")) {
            s = rs.getBigDecimal(fieldName);
        } else if (fieldClassName.equals("java.lang.Object") || fieldClassName.equals("oracle.sql.STRUCT")) {
            s = rs.getObject(fieldName);
        } else if (fieldClassName.equals("java.sql.Array") || fieldClassName.equals("oracle.sql.ARRAY")) {
            s = rs.getArray(fieldName);
        } else if (fieldClassName.equals("java.sql.Clob")) {
            s = rs.getClob(fieldName);
        } else if (fieldClassName.equals("java.sql.Blob")) {
            s = rs.getBlob(fieldName);
        } else {
            // 对于其它任何未知类型的处理
            s = rs.getObject(fieldName);
        }

        if (rs.wasNull()) {
            return null;
        } else {
            return s;
        }
    }


    /**
     * 如果多条sql不一样会生成多个ps
     * 实现原理跟mybatis的batchExecutor的batchUpdate一样
     * 调用时最好开启事物 如果有多个ps的情况不是一批插入的
     *
     * @param list
     * @return
     */
    public int[] insert(Collection<?> list) {

        List<String> sqls = new ArrayList<>();
        List<List<Object>> params = new ArrayList<>();
        for (Object obj : list) {
            Insert0Sql insert0Sql = new Insert0Sql(obj).invoke();
            sqls.add(insert0Sql.getSql());
            params.add(insert0Sql.getParamList());
        }
        return batchExecute(sqls, params);
    }

    /**
     * 批量执行语句
     * 需要在jdbc连接加上这句话才更快 rewriteBatchedStatements=true
     * 10万条数据插入1.5秒，修改9秒
     * insert返回是-2
     */
    @SneakyThrows
    public int[] batchExecute(List<String> sqls, List<List<Object>> params) {
        Assert.notEmpty(sqls);

        Connection conn = getConn();
        Map<String, PreparedStatement> map = new HashMap<>();
        for (int i = 0; i < params.size(); i++) {
            String sql = sqls.get(i);
            List<Object> param = params.get(i);

            if (!map.containsKey(sql)) {
                // 减少ps个数
                PreparedStatement ps = map.get(sql);
                if (ps == null) {
                    ps = conn.prepareStatement(formatMarkSql(sql, param.size()));
                    map.put(sql, ps);
                }
            }
            PreparedStatement ps = map.get(sql);
            setPsParam(ps, param);
            ps.addBatch();
            // 打印sql
            if (builder.printSql) {
                if (builder.printDetailSql) {
                    log.info("sql ==> {}", formatDbSql(sql, param.toArray()));
                } else {
                    log.info("sql ==> {}", sql);
                }
            }
        }

        int[] results = new int[params.size()];
        int cur = 0;
        try {
            for (PreparedStatement ps : map.values()) {
                // 返回值是-2就不用了
                int[] ints = ps.executeBatch();
                for (int anInt : ints) {
                    results[cur++] = anInt;
                }
            }
        } finally {
            log.info("batch-ps-size|{}", map.size());
            close(conn, map.values());
        }
        return results;
    }


    /**
     * 多条sql的方式
     *
     * @param lqs
     * @return
     */
    public int[] update(Collection<Lq> lqs) {

        List<String> sqls = new ArrayList<>();
        List<List<Object>> params = new ArrayList<>();
        for (Lq lq : lqs) {
            Object[] updateParam = lq.getUpdateParam();
            String sql = formatMarkSql(lq.getUpdate(), updateParam.length);

            sqls.add(sql);
            params.add(ColUtil.array2List(lq.getUpdateParam()));
        }
        return batchExecute(sqls, params);
    }


    /**
     * 多条sql的方式
     *
     * @param list
     * @return
     */
    @SneakyThrows
    public int[] updateById(Collection<?> list) {
        List<String> sqls = new ArrayList<>();
        List<List<Object>> params = new ArrayList<>();
        for (Object obj : list) {
            Update0Sql update0Sql = new Update0Sql(obj, false).invoke();
            sqls.add(update0Sql.getSql());
            params.add(update0Sql.getParamList());
        }
        return batchExecute(sqls, params);
    }

    /**
     * 插入 返回自增id
     *
     * @param obj
     * @return
     */
    @SneakyThrows
    public long insert(Object obj) {

        Assert.isTrue(!(obj instanceof Collection), "obj cannot be collection");

        Insert0Sql insert0Sql = new Insert0Sql(obj).invoke();
        long id = executeInsert(insert0Sql.getSql(), insert0Sql.getParamList().toArray());

        String pk = getTablePk(obj.getClass());
        boolean pkAuto = isPkAuto(obj.getClass());
        if (pkAuto) {
            String pka = StrUtil.toCamelCase(pk);
            cn.hutool.core.util.ReflectUtil.setFieldValue(obj, pka, id);
        }

        return id;
    }


    /**
     * 存在修改 不存在插入
     *
     * @return
     */
    @SneakyThrows
    public int insertOrUpdate(Object o, Lq existLq) {
        return insertOrUpdate(o, existLq, true);
    }

    /**
     * 不存在才插入
     *
     * @return
     */
    @SneakyThrows
    public int insertWhenNot(Object o, Lq existLq) {
        return insertOrUpdate(o, existLq, false);
    }

    /**
     * 插入
     *
     * @return
     */
    @SneakyThrows
    private int insertOrUpdate(Object o, Lq existLq, boolean existUpdate) {
        boolean existKey = exist(existLq);
        if (existKey) {
            log.info("insertOrUpdate-exist");
            if (!existUpdate) {
                return -1;
            }

            Lq updateLq = ObjectUtil.cloneByStream(existLq);

            BeanDesc beanDesc = BeanUtil.getBeanDesc(o.getClass());
            List<Lq.Item> setItems = updateLq.getSetItems();

            List<Lq.Item> whereItems = updateLq.getWhereItems();
            for (BeanDesc.PropDesc prop : beanDesc.getProps()) {
                String fieldName = prop.getFieldName();
                String dbName = StrUtil.toUnderlineCase(fieldName);


                // 排除where语句中的字段
                Optional<Lq.Item> any = whereItems.stream().filter(v -> v.getName().equals(dbName)).findAny();
                if (!any.isPresent()) {
                    // 增加set语句
                    Object fieldValue = BeanUtil.getFieldValue(o, fieldName);
                    // 不插入null字段 才可以用数据库默认的
                    if (fieldValue == null) {
                        continue;
                    }
                    setItems.add(new Lq.Item(dbName, fieldValue, EqType.EQ, false));
                }
            }
            return update(updateLq);
        } else {
            insert(o);
            return 1;
        }
    }

    @SneakyThrows
    public int delete(Lq lq) {
        if (CollUtil.isEmpty(lq.getWhereItems())) {
            // 危险操作不允许无where条件执行
            log.warn("not support not where delete");
            return -1;
        }
        return executeUpdate(lq.getDelete(), lq.getUpdateParam());
    }

    public int update(Lq lq) {

        if (lq.getSetItems().isEmpty()) {
            // 没执行修改操作
            return -1;
        }
        return executeUpdate(lq.getUpdate(), lq.getUpdateParam());
    }

    /**
     * 如果对象有通过数据库设置的更新update_time会失效 ,还有覆盖更新的问题
     * 不推荐用这个方法
     * @param obj
     * @return
     */
    public int updateById(Object obj) {
        return updateById(obj, false);
    }


    public int updateById(Object obj, boolean update2Null) {
        Assert.isTrue(!(obj instanceof Collection), "obj cannot be collection");

        Update0Sql update0Sql = new Update0Sql(obj, update2Null).invoke();
        List<Object> paramList = update0Sql.getParamList();

        return executeUpdate(update0Sql.getSql(), paramList.toArray());
    }


    /**
     * 通过主键查询
     *
     * @return
     */

    public <T> T queryById(Class<T> clazz, Long id) {

        String tname = getTableName(clazz);
        String pk = getTablePk(clazz);

        StringBuilder sb = new StringBuilder("select");
        BeanDesc beanDesc = BeanUtil.getBeanDesc(clazz);

        Iterator<BeanDesc.PropDesc> iterator = beanDesc.getProps().iterator();
        boolean b = false;
        while (iterator.hasNext()) {

            BeanDesc.PropDesc next = iterator.next();
            if (!SqlUtil.isSelectType(next)) {
                continue;
            }
            String fieldName = next.getFieldName();

            sb.append("`").append(StrUtil.toUnderlineCase(fieldName)).append("`").append(",");
            b = true;
        }
        if (b) {
            sb.setLength(sb.length() - 1);
        }
        sb.append(" from ").append(tname).append(" where ").append(pk).append(" = ?");

        return queryOneB(sb.toString(), clazz, new Object[]{id});
    }


    /**
     * 查询是否存在 行数>0
     *
     * @param sql
     * @param o
     * @return
     */
    @SneakyThrows
    public long count(String sql, Object... o) {
        sql = formatMarkSql(sql, o.length);

        Assert.isTrue(sql.contains("count"), "not exist count|{}", sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            st = getPs(conn, sql, o);
            rs = st.executeQuery();

            if (rs.next()) {
                return rs.getLong(1);
            }

        } finally {
            close(rs, st, conn);
        }
        throw new RuntimeException("not record");
    }

    public boolean exist(String sql, Object... o) {
        return count(sql, o) > 0;
    }

    public boolean exist(Lq lq) {
        String sql = lq.getCount();
        return exist(sql, lq.getSelectParam());
    }

    /**
     * 查询列表 返回map
     *
     * @param sql
     * @param param
     * @return
     */
    @SneakyThrows
    public List<Map> queryList(String sql, Object... param) {
        sql = formatMarkSql(sql, param.length);

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            ps = getPs(conn, sql, param);
            rs = ps.executeQuery();

            return rs2listmap(rs, null);

        } finally {
            close(rs, ps, conn);

        }
    }

    private String addLimit(String sql) {
        if (!sql.contains("limit")) {
            sql = sql + " limit 1";
        }
        return sql;
    }

    /**
     * 只返回一条记录
     *
     * @param sql
     * @return
     */
    @SneakyThrows
    public Map queryOne(String sql, Object... param) {
        sql = formatMarkSql(sql, param.length);
        sql = addLimit(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            st = getPs(conn, sql, param);
            rs = st.executeQuery();

            List<Map> lists = rs2listmap(rs, 1);
            return (lists == null || lists.isEmpty()) ? null : lists.get(0);

        } finally {
            close(rs, st, conn);

        }
    }


    @SneakyThrows
    public <T> T queryOne(Lq<T> lq) {
        return queryOneB(lq.getSelect(), lq.getTableClass(), lq.getSelectParam());
    }

    /**
     * 执行mark和ps格式化
     *
     * @param sql
     * @param param
     * @return
     */
    public static String formatMarkAndPsSql(String sql, Object[] param) {
        sql = formatMarkSql(sql, param.length);
        sql = formatDbSql(sql, param);
        return sql;
    }


    /**
     * 处理sql和 特殊参数
     */
    public static String formatMarkSql(String sql, int size) {
        Assert.notEmpty(sql, "sql cannot be empty");
        // 支持q?代替多个问号
        if (sql.contains(MARK_QUESTION)) {
            String question = StrUtil.repeat("?,", size);
            question = question.substring(0, question.length() - 1);
            sql = sql.replace(MARK_QUESTION, question);
        }

        return sql;
    }


    /**
     * 格式化sql 替换?成数据
     *
     * @param sql
     * @return
     */
    public static String formatDbSql(String sql, Object[] param) {
        // 将多个空个替换成一个空格
        sql = sql.replaceAll("\\s{1,}", " ");
        if (param == null) {
            return sql;
        } else {
            sql = Util.sqlParamReplace(sql, "?", param);
        }
        return sql;
    }


    /**
     * 设置参数
     *
     * @param sql
     * @param param
     * @throws SQLException
     */
    private PreparedStatement getPs(Connection conn, String sql, Object[] param) throws SQLException {
        // 创建语句执行者
        PreparedStatement st = conn.prepareStatement(sql);

        // 替换的参数
        if (builder.printSql) {
            if (builder.printDetailSql) {
                log.info("sql ==> {}", formatDbSql(sql, param));
            } else {
                log.info("sql ==> {}|{}", sql,param);
            }
        }
        setPsParam(st, param);
        return st;
    }

    @SneakyThrows
    private void setPsParam(PreparedStatement st, List<Object> params) {
        // 创建语句执行者
        if (params == null) {
            return;
        }
        //设置参数
        int i = 1;
        for (Object o1 : params) {
            // 如果是枚举
            if (o1 instanceof Enum) {
                o1 = o1 == null ? null : o1.toString();
            }
            st.setObject(i++, o1);
        }
    }

    private void setPsParam(PreparedStatement st, Object[] param) {
        if (param == null || param.length == 0) {
            return;
        }
        setPsParam(st, ColUtil.array2List(param));
    }

    /**
     * 只查询一个值
     */
    @SneakyThrows
    public Object queryOneColumn(String sql, Object... param) {
        sql = formatMarkSql(sql, param.length);

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            ps = getPs(conn, sql, param);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getObject(1);
            } else {
                return null;
            }


        } finally {
            close(rs, ps, conn);
        }
    }


    /**
     * 查询返回map ,以mapKey作为key
     * 需要mapKey是唯一的
     *
     * @param sql
     * @param mapKey
     * @return
     */
    public Map<Object, Map> queryMap(String sql, String mapKey, Object... param) {
        return queryMap(sql, mapKey, null, param);
    }


    /**
     * 同queryMap
     *
     * @param sql
     * @param mapKey
     * @param predicate 自定义key的格式
     * @return
     */
    @SneakyThrows
    public Map<Object, Map> queryMap(String sql, String mapKey, Predicate predicate, Object... param) {
        sql = formatMarkSql(sql, param.length);

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            ps = getPs(conn, sql, param);
            rs = ps.executeQuery();
            LinkedHashMap records = null;

            ResultSetMetaData rsmd = rs.getMetaData();
            int fieldCount = rsmd.getColumnCount();
            records = new LinkedHashMap();
            while (rs.next()) {
                Map<String, Object> valueMap = new LinkedHashMap<>();
                for (int i = 1; i <= fieldCount; i++) {
                    String fieldClassName = rsmd.getColumnClassName(i);
                    //获取别名
                    String fieldName = rsmd.getColumnLabel(i);
                    recordMappingToMap(fieldClassName, fieldName, rs, valueMap);
                }
                Object ss = valueMap.get(mapKey);
                //转换key值
                if (predicate != null) {
                    ss = predicate.transfer(ss);
                }

                if (records.containsKey(ss)) {
                    log.warn("exist map by Covered,have Repeated data:{}", ss);
                }
                records.put(ss, valueMap);
            }

            return records;

        } finally {
            close(rs, ps, conn);

        }

    }

    /**
     * @param rs
     * @param size 获取记录数量
     * @return
     */
    @SneakyThrows
    private List<Map> rs2listmap(ResultSet rs, Integer size) {

        int j = 0;
        List records = null;

        ResultSetMetaData rsmd = rs.getMetaData();
        int fieldCount = rsmd.getColumnCount();
        records = new ArrayList();
        while (rs.next()) {
            if (size != null) {
                if (j >= size) {
                    break;
                }
                j++;
            }
            Map<String, String> valueMap = new LinkedHashMap<String, String>();
            for (int i = 1; i <= fieldCount; i++) {
                String fieldClassName = rsmd.getColumnClassName(i);
                //获取别名
                String fieldName = rsmd.getColumnLabel(i);
                recordMappingToMap(fieldClassName, fieldName, rs, valueMap);
            }
            records.add(valueMap);
        }
        return records;
    }

    public interface Predicate {
        String transfer(Object sourceKey);
    }


    /**
     * 开启事物
     * 返回true提交事物
     *
     * @param fun
     */
    @SneakyThrows
    public boolean tx(Func0<Boolean> fun) {
        Connection conn = null;
        try {
            conn = getConn();
            threadConn.set(conn);
            conn.setAutoCommit(false);
            boolean result = fun.call();
            if (result) {
                conn.commit();
            } else {
                conn.rollback();
            }
            return result;
        } catch (Throwable e) {
            conn.rollback();
            throw e;
        } finally {
            conn.setAutoCommit(true);
            threadConn.set(null);
            close(null, null, conn);
        }
    }


    /**
     * 执行 insert或 update方法
     *
     * @param sql
     * @param param
     * @return
     */
    @SneakyThrows
    public int executeUpdate(String sql, Object... param) {
        sql = formatMarkSql(sql, param.length);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            // 创建语句执行者
            ps = getPs(conn, sql, param);
            int n = ps.executeUpdate();
            return n;
        } catch (Exception e) {
            log.error("executeUpdate|{}|{}", sql, param);
            //       FileUtil.writeString(sql, "/Users/mac126/Documents/sjmm-capture-jd/aa.sql", "utf-8");
            throw e;
        } finally {
            close(rs, ps, conn);
        }
    }

    /**
     * 执行 insert 返回自增id
     */
    @SneakyThrows
    public long executeInsert(String sql, Object... param) {
        sql = formatMarkSql(sql, param.length);
        Assert.isTrue(sql.startsWith("insert"), "sql need start with insert");
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();

            // 创建语句执行者
            ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            setPsParam(ps, param);
            int n = ps.executeUpdate();
            ResultSet rst = ps.getGeneratedKeys();
            if (rst.next()) {
                return rst.getLong(1);
            } else {
                return 0;
            }
        } catch (Exception e) {
            log.error("executeUpdate|{}|{}", sql, param);
            //       FileUtil.writeString(sql, "/Users/mac126/Documents/sjmm-capture-jd/aa.sql", "utf-8");
            throw e;
        } finally {
            close(rs, ps, conn);
        }
    }


    public String getDatabaseName() {
        return databaseName;
    }


    /**
     * 查询map value是bean
     */
    @SneakyThrows
    public <T> Map<Object, List<T>> queryMapListB(String sql, String[] param, Class<T> type, String mapKey) {
        sql = formatMarkSql(sql, param.length);


        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            ps = getPs(conn, sql, param);
            rs = ps.executeQuery();

            Map<Object, List<T>> stringListMap = rs2mapListBean(rs, mapKey, type);

            return stringListMap;


        } finally {
            close(rs, ps, conn);

        }
    }


    public <T> List<T> queryList(Lq<T> lq) {
        if (lq.isNotExcute()) {
            return new ArrayList<>();
        }
        return queryListB(lq.getSelect(), lq.getTableClass(), lq.getSelectParam());
    }


    /**
     * 查询一个返回bean
     */
    public <T> T queryOneB(String sql, Class<T> type, Object... param) {
        sql = addLimit(sql);

        List<T> lists = queryListB(sql, type, param);
        return (lists == null || lists.isEmpty()) ? null : lists.get(0);
    }


    /**
     * 查询list返回bean
     */
    @SneakyThrows
    public <T> List<T> queryListB(String sql, Class<T> type, Object... param) {
        sql = formatMarkSql(sql, param.length);

        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = getConn();
            ps = getPs(conn, sql, param);
            rs = ps.executeQuery();

            List<T> list = new ArrayList<>();
            //使用mybatis的对象注入方法 ，java自带的反射效率低，内省只能识别返回值是void的set方法
            ReflectUtil0.Reflector forClass = rfFactory.findForClass(type);

            ResultSetMetaData rsmd = rs.getMetaData();
            int fieldCount = rsmd.getColumnCount();

            if (isBaseType(type)) {
                Assert.isTrue(fieldCount == 1, "{} is baseType only 1 column", type);
                while (rs.next()) {
                    for (int i = 1; i <= fieldCount; i++) {
                        Object value = this.processColumn(rs, i, type);
                        list.add((T) value);
                    }
                }
            } else {
                while (rs.next()) {
                    //通过反射new一个对象
                    T bean = type.getDeclaredConstructor().newInstance();
                    list.add(bean);

                    for (int i = 1; i <= fieldCount; i++) {
                        //获取对象的类型
                        String fieldClassName = rsmd.getColumnClassName(i);
                        //获取别名
                        String fieldName = rsmd.getColumnLabel(i);

                        String camelFname = underlineToCamel(fieldName);

                        if (forClass.hasSetter(camelFname)) {
                            ReflectUtil0.Invoker setInvoker = forClass.getSetInvoker(camelFname);
                            Field field = ReflectUtil.getField(type, camelFname);

                            //判断这个属性是什么数据类型,匹配并返回
                            Object value = this.processColumn(rs, i, setInvoker.getType());
                            if (field.getAnnotation(JsonData.class) != null) {
                                Object o = JSON.parseObject((String) value, setInvoker.getType());
                                setInvoker.invoke(bean, new Object[]{o});
                            }else{
                                setInvoker.invoke(bean, new Object[]{value});
                            }
                        }
                    }
                }
            }

            return list;

        } finally {
            close(rs, ps, conn);

        }
    }


    private static boolean isBaseType(Class<?> clazz) {
        //如果是byte类型(包含基本类型和包装类)
        if (clazz == byte.class || clazz == Byte.class) {
            return true;
        }
        //如果是short类型(包含基本类型和包装类)
        if (clazz == short.class || clazz == Short.class) {
            return true;
        }
        //如果是char类型(包含基本类型和包装类)
        if (clazz == char.class || clazz == Character.class) {
            return true;
        }
        //如果是整型(包含基本类型和包装类)
        if (clazz == int.class || clazz == Integer.class) {
            //为属性赋值
            return true;
        }
        //如果是float(包含基本类型和包装类)
        if (clazz == float.class || clazz == Float.class) {
            //为属性赋值
            return true;
        }
        //如果是double(包含基本类型和包装类)
        if (clazz == double.class || clazz == Double.class) {
            //为属性赋值
            return true;
        }
        //如果是double(包含基本类型和包装类)
        if (clazz == long.class || clazz == Long.class) {
            //为属性赋值
            return true;
        }
        //如果是boolean(包含基本类型和包装类)
        if (clazz == boolean.class || clazz == Boolean.class) {
            return true;
        }
        //如果是boolean(包含基本类型和包装类)
        if (clazz == String.class) {
            //为属性赋值
            return true;
        }
        //如果是日期类型
        if (clazz == Date.class) {
            return true;
        }
        return false;
    }

    private ReflectUtil0.DefaultReflectorFactory rfFactory = new ReflectUtil0.DefaultReflectorFactory();


    private <T> Map<Object, List<T>> rs2mapListBean(ResultSet rs, String mapKey, Class<T> type) throws
            Exception {


        //使用mybatis的对象注入方法 ，java自带的反射效率低，内省只能识别返回值是void的set方法
        ReflectUtil0.Reflector forClass = rfFactory.findForClass(type);

        Map<Object, List<T>> records = null;

        ResultSetMetaData rsmd = rs.getMetaData();
        int fieldCount = rsmd.getColumnCount();

        records = new LinkedHashMap();

        String mapKeyClassName = null;
        while (rs.next()) {
            //通过反射new一个对象
            T bean = type.newInstance();

            for (int i = 1; i <= fieldCount; i++) {
                //获取对象的类型
                String fieldClassName = rsmd.getColumnClassName(i);
                //获取别名
                String fieldName = rsmd.getColumnLabel(i);

                String camelFname = underlineToCamel(fieldName);

                if (forClass.hasSetter(camelFname)) {
                    ReflectUtil0.Invoker setInvoker = forClass.getSetInvoker(camelFname);

                    //判断这个属性是什么数据类型,匹配并返回
                    Object value = this.processColumn(rs, i, setInvoker.getType());
                    setInvoker.invoke(bean, new Object[]{value});
                }

                //获取mapKey的类型
                if (fieldName.equals(mapKey)) {
                    mapKeyClassName = fieldClassName;
                }
            }

            if (mapKeyClassName == null) {
                throw new RuntimeException("sql no mapKey" + mapKey);
            }

            Object mapKeyVal = getFiledVal(mapKeyClassName, mapKey, rs);

            if (mapKeyVal == null) {
                log.warn("get mapKey is null {}", mapKey);
                continue;
            }

            if (!records.containsKey(mapKeyVal)) {
                List<T> list = new ArrayList();
                records.put(mapKeyVal, list);
            }
            records.get(mapKeyVal).add(bean);
        }


        return records;
    }


    private Object processColumn(ResultSet rs, int index, Class<?> propType) throws SQLException {

        //如果该属性不是一个基本类型并且该列的值为null
        //返回一个null
        if (!propType.isPrimitive() && rs.getObject(index) == null) {
            return null;
        }

        if (propType.equals(String.class)) {
            return rs.getString(index);

        } else if (propType.equals(Integer.TYPE) || propType.equals(Integer.class)) {
            return Integer.valueOf(rs.getInt(index));

        } else if (propType.equals(Boolean.TYPE) || propType.equals(Boolean.class)) {
            return Boolean.valueOf(rs.getBoolean(index));

        } else if (propType.equals(Long.TYPE) || propType.equals(Long.class)) {
            return Long.valueOf(rs.getLong(index));

        } else if (propType.equals(Double.TYPE) || propType.equals(Double.class)) {
            return Double.valueOf(rs.getDouble(index));

        } else if (propType.equals(Float.TYPE) || propType.equals(Float.class)) {
            return Float.valueOf(rs.getFloat(index));

        } else if (propType.equals(Short.TYPE) || propType.equals(Short.class)) {
            return Short.valueOf(rs.getShort(index));

        } else if (propType.equals(Byte.TYPE) || propType.equals(Byte.class)) {
            return Byte.valueOf(rs.getByte(index));

        } else if (propType.equals(Timestamp.class)) {
            return rs.getTimestamp(index);

        } else if (propType.equals(LocalDateTime.class)) {
            return dateToLocalDateTime(new Date(rs.getDate(index).getTime()));

        } else if (propType.isEnum()) {
            String s = rs.getString(index);
            return s == null ? null : Util.getEnumObj(propType, s);
        } else {
            return rs.getObject(index);
        }
    }

    public static LocalDateTime dateToLocalDateTime(Date time) {
        Instant it = time.toInstant();
        ZoneId zid = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(it, zid);
    }

    /**
     * 分页执行函数
     */
    public interface IjdbcPage {

        /**
         * @param limit 分页查询 limit是分页参数字符串
         * @param page  当前第几页
         * @return
         */
        boolean run(String limit, int page);
    }


    /**
     * 分页执行器
     *
     * @param page     从1开始
     * @param size     每页大小
     * @param jdbcPage 回调方法
     */
    public static void page(int page, int size, IjdbcPage jdbcPage) {

        while (true) {
            String limit = " limit " + (page - 1) * size + "," + size;
            boolean b = jdbcPage.run(limit, page);
            if (!b) {
                break;
            }
            page++;
        }
    }


    /**
     * 下划线 转 驼峰
     *
     * @param param
     * @return
     */
    private static String underlineToCamel(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == '_') {
                if (++i < len) {
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }


    static class Util {
        static final int INDEX_NOT_FOUND = -1;

        static boolean isEmpty(final CharSequence cs) {
            return cs == null || cs.length() == 0;
        }


        private static String sqlParamReplace(final String text, String searchString, final Object[] replacements) {
            if (isEmpty(text) || isEmpty(searchString) || replacements == null || replacements.length == 0) {
                return text;
            }
            String searchText = text;

            int start = 0;
            int end = searchText.indexOf(searchString, start);
            if (end == INDEX_NOT_FOUND) {
                return text;
            }
            final int replLength = searchString.length();

            final StringBuilder buf = new StringBuilder(text.length() + replacements.length * 10);
            int i = 0;
            while (end != INDEX_NOT_FOUND) {
                if (i + 1 > replacements.length) {
                    throw new IndexOutOfBoundsException("replacements size not enough");
                }
                buf.append(text, start, end);
                buf.append(SqlUtil.wrapObj(replacements[i]));
                i++;
                start = end + replLength;

                end = searchText.indexOf(searchString, start);
            }
            buf.append(text, start, text.length());
            return buf.toString();
        }


        /**
         * 通过s获取class
         *
         * @param z
         * @param s
         * @return
         */
        public static Object getEnumObj(Class z, String s) {

            Object[] objects = z.getEnumConstants();
            for (Object object : objects) {
                if (object.toString().equals(s)) {
                    return object;
                }
            }
            return null;
        }

    }


    private class Insert0Sql {
        private Object obj;
        private String sql;
        private List<Object> paramList;

        public Insert0Sql(Object obj) {
            this.obj = obj;
        }

        public String getSql() {
            return sql;
        }

        public List<Object> getParamList() {
            return paramList;
        }

        public Insert0Sql invoke() {
            String tname = getTableName(obj.getClass());
            String pk = getTablePk(obj.getClass());
            boolean pkAuto = isPkAuto(obj.getClass());
            String pka = StrUtil.toCamelCase(pk);

            StringBuilder sb = new StringBuilder("insert into " + tname + "(");
            BeanDesc beanDesc = BeanUtil.getBeanDesc(obj.getClass());

            paramList = new ArrayList<>();
            Iterator<BeanDesc.PropDesc> iterator = beanDesc.getProps().iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                BeanDesc.PropDesc next = iterator.next();
                String fieldName = next.getFieldName();
                if (!SqlUtil.isSelectType(next)) {
                    continue;
                }
                if (pka.equals(fieldName) && pkAuto) {
                    continue;
                }
                Object fieldValue = BeanUtil.getFieldValue(obj, fieldName);
                // 不插入null字段 才可以用数据库默认的
                if (fieldValue == null) {
                    continue;
                }
                paramList.add(fieldValue);

                sb.append("`").append(StrUtil.toUnderlineCase(fieldName)).append("`").append(",");
                b = true;
            }
            if (b) {
                sb.setLength(sb.length() - 1);
            }
            sb.append(") values(q?)");
            sql = sb.toString();
            return this;
        }
    }

    private class Update0Sql {
        private Object obj;
        private boolean update2Null;
        private String sql;
        private List<Object> paramList;

        public Update0Sql(Object obj, boolean update2Null) {
            this.obj = obj;
            this.update2Null = update2Null;
        }

        public String getSql() {
            return sql;
        }

        public List<Object> getParamList() {
            return paramList;
        }

        public Update0Sql invoke() {
            String tname = getTableName(obj.getClass());
            String pk = getTablePk(obj.getClass());
            String pka = StrUtil.toCamelCase(pk);

            StringBuilder sb = new StringBuilder("update  " + tname + " set ");
            BeanDesc beanDesc = BeanUtil.getBeanDesc(obj.getClass());

            paramList = new ArrayList<>();
            Iterator<BeanDesc.PropDesc> iterator = beanDesc.getProps().iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                BeanDesc.PropDesc next = iterator.next();
                String fieldName = next.getFieldName();
                if (!SqlUtil.isSelectType(next)) {
                    continue;
                }
                if (pka.equals(fieldName)) {
                    continue;
                }
                Object fieldValue = BeanUtil.getFieldValue(obj, fieldName);
                // 不插入null字段 才可以用数据库默认的
                if (fieldValue == null) {
                    if (!update2Null) {
                        continue;
                    }
                }
                paramList.add(fieldValue);

                sb.append("`").append(StrUtil.toUnderlineCase(fieldName)).append("`").append("=").append("?");
                sb.append(",");
                b = true;
            }
            if (b) {
                sb.setLength(sb.length() - 1);
            }

            sb.append(" where ").append(pk).append(" = ?");
            paramList.add(BeanUtil.getFieldValue(obj, pka));
            sql = sb.toString();
            return this;
        }
    }
}