package cn.xinyanli.db.pool;

import cn.xinyanli.core.util.ObjUtil;
import cn.xinyanli.db.JdbcUtil;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;


@Slf4j
public class DataSourceImpl implements IConnectionPool {

    private ReentrantLock lock = new ReentrantLock();

    //定义连接池中连接对象的存储容器
    private List<PoolConnection> list = Collections.synchronizedList(new ArrayList<>());


    Config config;

    public DataSourceImpl(Config config) {
        ObjUtil.getRequired(config.url,"db.url");
        ObjUtil.getRequired(config.username,"db.username");
        ObjUtil.getRequired(config.password,"db.password");
        this.config = config;

        // 初始化
        releaseConnection(getConnection());
    }

    @Override
    public Connection getConnection() {
        PoolConnection poolConnection = null;

        try {
            lock.lock();

            //连接池对象为空时，初始化连接对象
            if (list.size() == 0) {
                createConnection(config.initSize);
            }

            //获取可用连接对象
            poolConnection = getAvailableConnection();

            //没有可用连接对象时，等待连接对象的释放或者创建新的连接对象使用
            while (poolConnection == null) {
                log.info("---------------等待连接---------------");
                createConnection(config.stepSize);
                poolConnection = getAvailableConnection();

                if (poolConnection == null) {
                    TimeUnit.MILLISECONDS.sleep(30);
                }
            }

        } catch (Exception e) {
            log.error("getDataSource", e);
        } finally {
            lock.unlock();
        }
        return poolConnection.getConnect();
    }


    @Override
    public void releaseConnection(Connection connection) {
        for (PoolConnection pool : list) {
            if (pool.getConnect() == connection) {
                pool.releaseConnect();
            }
        }
    }


    //创建数据库连接
    private void createConnection(int count) {

        try {
            // 注册驱动
            Class.forName(config.driverName);

            if (list.size() + count <= config.maxSize) {
                for (int i = 0; i < count; i++) {

                    // 获取连接
                    Connection connection = DriverManager.getConnection(
                            config.url,
                            config.username,
                            config.password
                    );
                    PoolConnection pool = new PoolConnection(connection, true);
                    list.add(pool);
                    log.info("初始化了第{}个连接hash={}", (i + 1), connection.hashCode());
                }
            }

        } catch (Exception e) {
            log.error("newConnection", e);
            return;
        }

    }

    //获取可用连接对象
    @SneakyThrows
    private PoolConnection getAvailableConnection() {
        for (PoolConnection pool : list) {
            if (pool.isStatus()) {
                Connection con = pool.getConnect();
                if (!con.isValid(config.timeout)) {
                    log.info("getAvailableConnection-isValid-false");
                    Connection connection = DriverManager.getConnection(
                            config.url,
                            config.username,
                            config.password
                    );
                    pool.setConnect(connection);
                    log.info("连接已失效，新建连接hash={}",  connection.hashCode());

                }
                pool.setStatus(false);
                return pool;
            }
        }

        return null;
    }


    @Slf4j
    static class PoolConnection {

        private Connection connect;
        //false--繁忙，true--空闲
        private boolean status;


        public PoolConnection(Connection connect, boolean status) {
            this.connect = connect;
            this.status = status;
        }

        public Connection getConnect() {
            return connect;
        }

        public void setConnect(Connection connect) {
            this.connect = connect;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        //释放连接池中的连接对象
        public void releaseConnect() {
   //         log.info("-----------释放连接-----------hash={}", this.connect.hashCode());
            this.status = true;
        }

    }

    @Data
    @Accessors(chain = true)
    public static class Config {
        String driverName;
        String url;
        String username;
        String password;

        //定义默认连接池属性配置
        private int initSize = 2;
        private int maxSize = 4;
        private int stepSize = 1;
        /**
         * 单位秒
         */
        private int timeout = 1;

    }
}