package cn.xinyanli.db.pool;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectionPool  {
    /**
     * 获取连接
     * @return
     */
    Connection getConnection()  ;

    /**
     * 释放连接
     * @param connection
     */
    public void releaseConnection(Connection connection);
}