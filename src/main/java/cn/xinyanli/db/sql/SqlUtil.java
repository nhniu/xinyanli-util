package cn.xinyanli.db.sql;

import cn.hutool.core.bean.BeanDesc;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.xinyanli.core.util.RefUtil;
import cn.xinyanli.db.annotation.JsonData;
import cn.xinyanli.db.annotation.TableInfo;
import com.alibaba.nacos.client.naming.utils.CollectionUtils;

import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

public class SqlUtil {

    /**
     * 生成数据库用于 in 查询的 id 字符串
     */
    public static String createIn(Collection<?> ids) {
        return CollectionUtils.isEmpty(ids) ? null : ids.stream().filter(o -> Objects.nonNull(o)).map(b -> b.toString()).collect(Collectors.joining("','", "('", "')"));
    }

    /**
     * 包装类型 字符串加单引号
     *
     * @param obj
     */
    public static String wrapObj(Object obj) {
        if (obj == null) {
            return "null";
        } else if (obj instanceof Number || obj instanceof Boolean) {
            return String.valueOf(obj);
        } else if (obj instanceof Date) {
            return StringEscape.escapeString(DateUtil.format((Date) obj, DatePattern.NORM_DATETIME_PATTERN));
        } else {
            return StringEscape.escapeString(obj.toString());
        }
    }


    /**
     * 是否是支持的列类型
     */
    public static boolean isSelectType(BeanDesc.PropDesc prop) {
        Class<?> obj = prop.getFieldClass();
        return obj.equals(Integer.class) ||
                obj.equals(Byte.class) ||
                obj.equals(Long.class) ||
                obj.equals(Double.class) ||
                obj.equals(Float.class) ||
                obj.equals(Character.class) ||
                obj.equals(Short.class) ||
                obj.isEnum() ||
                obj.equals(Boolean.class) || obj.equals(String.class) || obj.equals(Character.class)
                || obj.equals(BigDecimal.class)
                || obj.equals(Date.class) || obj.equals(LocalDateTime.class) || obj.equals(Time.class) || obj.equals(Timestamp.class) ||
                prop.getField().getAnnotation(JsonData.class) != null;
    }


    public static String getTableName(Class clazz) {
        TableInfo annotation = (TableInfo) clazz.getAnnotation(TableInfo.class);
        return annotation.value();
    }

    public static String getTablePk(Class clazz) {
        TableInfo annotation = (TableInfo) clazz.getAnnotation(TableInfo.class);
        if (annotation == null) {
            return "id";
        }
        return annotation.pk();
    }

    public static boolean isPkAuto(Class clazz) {
        TableInfo annotation = (TableInfo) clazz.getAnnotation(TableInfo.class);
        if (annotation == null) {
            return true;
        }
        return annotation.pkAuto();
    }
}
