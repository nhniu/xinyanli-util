package cn.xinyanli.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * json数据类型
 * @author hewei
 * @date 2020/2/12 17:39
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TableInfo {

    String value() default "";

    String pk() default "id";
    boolean pkAuto() default true;
}