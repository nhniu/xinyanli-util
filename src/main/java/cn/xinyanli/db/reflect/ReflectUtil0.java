package cn.xinyanli.db.reflect;

import lombok.Data;

import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 参考mybatis的反射工具方法
 * bean的注入
 *
 * @author hewei
 * @date 2019/5/9 9:50
 **/
public class ReflectUtil0 {

    @Data
    public static class DefaultReflectorFactory {
        private final ConcurrentMap<Class<?>, Reflector> reflectorMap = new ConcurrentHashMap<>();

        public Reflector findForClass(Class<?> type) {
            Reflector cached = reflectorMap.get(type);
            if (cached == null) {
                cached = new Reflector(type);
                reflectorMap.put(type, cached);
            }
            return cached;
        }
    }

    public static class Reflector {

        private final Class<?> type;
        private final String[] readablePropertyNames;
        private final String[] writeablePropertyNames;
        private final Map<String, Invoker> setMethods = new HashMap<>();
        private final Map<String, Invoker> getMethods = new HashMap<>();
        private final Map<String, Class<?>> setTypes = new HashMap<>();
        private final Map<String, Class<?>> getTypes = new HashMap<>();
        private Constructor<?> defaultConstructor;

        private Map<String, String> caseInsensitivePropertyMap = new HashMap<>();

        public Reflector(Class<?> clazz) {
            type = clazz;
            addDefaultConstructor(clazz);
            addGetMethods(clazz);
            addSetMethods(clazz);
            addFields(clazz);
            readablePropertyNames = getMethods.keySet().toArray(new String[getMethods.keySet().size()]);
            writeablePropertyNames = setMethods.keySet().toArray(new String[setMethods.keySet().size()]);
            for (String propName : readablePropertyNames) {
                caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
            }
            for (String propName : writeablePropertyNames) {
                caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
            }
        }

        private void addDefaultConstructor(Class<?> clazz) {
            Constructor<?>[] consts = clazz.getDeclaredConstructors();
            for (Constructor<?> constructor : consts) {
                if (constructor.getParameterTypes().length == 0) {
                    if (canAccessPrivateMethods()) {
                        try {
                            constructor.setAccessible(true);
                        } catch (Exception e) {
                            // Ignored. This is only a final precaution, nothing we can do.
                        }
                    }
                    if (constructor.isAccessible()) {
                        this.defaultConstructor = constructor;
                    }
                }
            }
        }

        private void addGetMethods(Class<?> cls) {
            Map<String, List<Method>> conflictingGetters = new HashMap<>();
            Method[] methods = getClassMethods(cls);
            for (Method method : methods) {
                if (method.getParameterTypes().length > 0) {
                    continue;
                }
                String name = method.getName();
                if ((name.startsWith("get") && name.length() > 3)
                        || (name.startsWith("is") && name.length() > 2)) {
                    name = PropertyNamer.methodToProperty(name);
                    addMethodConflict(conflictingGetters, name, method);
                }
            }
            resolveGetterConflicts(conflictingGetters);
        }

        private void resolveGetterConflicts(Map<String, List<Method>> conflictingGetters) {
            for (Map.Entry<String, List<Method>> entry : conflictingGetters.entrySet()) {
                Method winner = null;
                String propName = entry.getKey();
                for (Method candidate : entry.getValue()) {
                    if (winner == null) {
                        winner = candidate;
                        continue;
                    }
                    Class<?> winnerType = winner.getReturnType();
                    Class<?> candidateType = candidate.getReturnType();
                    if (candidateType.equals(winnerType)) {
                        if (!boolean.class.equals(candidateType)) {
                            throw new ReflectionException(
                                    "Illegal overloaded getter method with ambiguous type for property "
                                            + propName + " in class " + winner.getDeclaringClass()
                                            + ". This breaks the JavaBeans specification and can cause unpredictable results.");
                        } else if (candidate.getName().startsWith("is")) {
                            winner = candidate;
                        }
                    } else if (candidateType.isAssignableFrom(winnerType)) {
                        // OK getter type is descendant
                    } else if (winnerType.isAssignableFrom(candidateType)) {
                        winner = candidate;
                    } else {
                        throw new ReflectionException(
                                "Illegal overloaded getter method with ambiguous type for property "
                                        + propName + " in class " + winner.getDeclaringClass()
                                        + ". This breaks the JavaBeans specification and can cause unpredictable results.");
                    }
                }
                addGetMethod(propName, winner);
            }
        }

        private void addGetMethod(String name, Method method) {
            if (isValidPropertyName(name)) {
                getMethods.put(name, new MethodInvoker(method));
                Type returnType = TypeParameterResolver.resolveReturnType(method, type);
                getTypes.put(name, typeToClass(returnType));
            }
        }

        private void addSetMethods(Class<?> cls) {
            Map<String, List<Method>> conflictingSetters = new HashMap<String, List<Method>>();
            Method[] methods = getClassMethods(cls);
            for (Method method : methods) {
                String name = method.getName();
                if (name.startsWith("set") && name.length() > 3) {
                    if (method.getParameterTypes().length == 1) {
                        name = PropertyNamer.methodToProperty(name);
                        addMethodConflict(conflictingSetters, name, method);
                    }
                }
            }
            resolveSetterConflicts(conflictingSetters);
        }

        private void addMethodConflict(Map<String, List<Method>> conflictingMethods, String name, Method method) {
            List<Method> list = conflictingMethods.get(name);
            if (list == null) {
                list = new ArrayList<Method>();
                conflictingMethods.put(name, list);
            }
            list.add(method);
        }

        private void resolveSetterConflicts(Map<String, List<Method>> conflictingSetters) {
            for (String propName : conflictingSetters.keySet()) {
                List<Method> setters = conflictingSetters.get(propName);
                Class<?> getterType = getTypes.get(propName);
                Method match = null;
                ReflectionException exception = null;
                for (Method setter : setters) {
                    Class<?> paramType = setter.getParameterTypes()[0];
                    if (paramType.equals(getterType)) {
                        // should be the best match
                        match = setter;
                        break;
                    }
                    if (exception == null) {
                        try {
                            match = pickBetterSetter(match, setter, propName);
                        } catch (ReflectionException e) {
                            // there could still be the 'best match'
                            match = null;
                            exception = e;
                        }
                    }
                }
                if (match == null) {
                    throw exception;
                } else {
                    addSetMethod(propName, match);
                }
            }
        }

        private Method pickBetterSetter(Method setter1, Method setter2, String property) {
            if (setter1 == null) {
                return setter2;
            }
            Class<?> paramType1 = setter1.getParameterTypes()[0];
            Class<?> paramType2 = setter2.getParameterTypes()[0];
            if (paramType1.isAssignableFrom(paramType2)) {
                return setter2;
            } else if (paramType2.isAssignableFrom(paramType1)) {
                return setter1;
            }
            throw new ReflectionException("Ambiguous setters defined for property '" + property + "' in class '"
                    + setter2.getDeclaringClass() + "' with types '" + paramType1.getName() + "' and '"
                    + paramType2.getName() + "'.");
        }

        private void addSetMethod(String name, Method method) {
            if (isValidPropertyName(name)) {
                setMethods.put(name, new MethodInvoker(method));
                Type[] paramTypes = TypeParameterResolver.resolveParamTypes(method, type);
                setTypes.put(name, typeToClass(paramTypes[0]));
            }
        }

        private Class<?> typeToClass(Type src) {
            Class<?> result = null;
            if (src instanceof Class) {
                result = (Class<?>) src;
            } else if (src instanceof ParameterizedType) {
                result = (Class<?>) ((ParameterizedType) src).getRawType();
            } else if (src instanceof GenericArrayType) {
                Type componentType = ((GenericArrayType) src).getGenericComponentType();
                if (componentType instanceof Class) {
                    result = Array.newInstance((Class<?>) componentType, 0).getClass();
                } else {
                    Class<?> componentClass = typeToClass(componentType);
                    result = Array.newInstance(componentClass, 0).getClass();
                }
            }
            if (result == null) {
                result = Object.class;
            }
            return result;
        }

        private void addFields(Class<?> clazz) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (canAccessPrivateMethods()) {
                    try {
                        field.setAccessible(true);
                    } catch (Exception e) {
                        // Ignored. This is only a final precaution, nothing we can do.
                    }
                }
                if (field.isAccessible()) {
                    if (!setMethods.containsKey(field.getName())) {
                        // issue #379 - removed the check for final because JDK 1.5 allows
                        // modification of final fields through reflection (JSR-133). (JGB)
                        // pr #16 - final static can only be set by the classloader
                        int modifiers = field.getModifiers();
                        if (!(Modifier.isFinal(modifiers) && Modifier.isStatic(modifiers))) {
                            addSetField(field);
                        }
                    }
                    if (!getMethods.containsKey(field.getName())) {
                        addGetField(field);
                    }
                }
            }
            if (clazz.getSuperclass() != null) {
                addFields(clazz.getSuperclass());
            }
        }

        private void addSetField(Field field) {
            if (isValidPropertyName(field.getName())) {
                setMethods.put(field.getName(), new SetFieldInvoker(field));
                Type fieldType = TypeParameterResolver.resolveFieldType(field, type);
                setTypes.put(field.getName(), typeToClass(fieldType));
            }
        }

        private void addGetField(Field field) {
            if (isValidPropertyName(field.getName())) {
                getMethods.put(field.getName(), new GetFieldInvoker(field));
                Type fieldType = TypeParameterResolver.resolveFieldType(field, type);
                getTypes.put(field.getName(), typeToClass(fieldType));
            }
        }

        private boolean isValidPropertyName(String name) {
            return !(name.startsWith("$") || "serialVersionUID".equals(name) || "class".equals(name));
        }

        /*
         * This method returns an array containing all methods
         * declared in this class and any superclass.
         * We use this method, instead of the simpler Class.getMethods(),
         * because we want to look for private methods as well.
         *
         * @param cls The class
         * @return An array containing all methods in this class
         */
        private Method[] getClassMethods(Class<?> cls) {
            Map<String, Method> uniqueMethods = new HashMap<String, Method>();
            Class<?> currentClass = cls;
            while (currentClass != null && currentClass != Object.class) {
                addUniqueMethods(uniqueMethods, currentClass.getDeclaredMethods());

                // we also need to look for interface methods -
                // because the class may be abstract
                Class<?>[] interfaces = currentClass.getInterfaces();
                for (Class<?> anInterface : interfaces) {
                    addUniqueMethods(uniqueMethods, anInterface.getMethods());
                }

                currentClass = currentClass.getSuperclass();
            }

            Collection<Method> methods = uniqueMethods.values();

            return methods.toArray(new Method[methods.size()]);
        }

        private void addUniqueMethods(Map<String, Method> uniqueMethods, Method[] methods) {
            for (Method currentMethod : methods) {
                if (!currentMethod.isBridge()) {
                    String signature = getSignature(currentMethod);
                    // check to see if the method is already known
                    // if it is known, then an extended class must have
                    // overridden a method
                    if (!uniqueMethods.containsKey(signature)) {
                        if (canAccessPrivateMethods()) {
                            try {
                                currentMethod.setAccessible(true);
                            } catch (Exception e) {
                                // Ignored. This is only a final precaution, nothing we can do.
                            }
                        }

                        uniqueMethods.put(signature, currentMethod);
                    }
                }
            }
        }

        private String getSignature(Method method) {
            StringBuilder sb = new StringBuilder();
            Class<?> returnType = method.getReturnType();
            if (returnType != null) {
                sb.append(returnType.getName()).append('#');
            }
            sb.append(method.getName());
            Class<?>[] parameters = method.getParameterTypes();
            for (int i = 0; i < parameters.length; i++) {
                if (i == 0) {
                    sb.append(':');
                } else {
                    sb.append(',');
                }
                sb.append(parameters[i].getName());
            }
            return sb.toString();
        }

        private static boolean canAccessPrivateMethods() {
            try {
                SecurityManager securityManager = System.getSecurityManager();
                if (null != securityManager) {
                    securityManager.checkPermission(new ReflectPermission("suppressAccessChecks"));
                }
            } catch (SecurityException e) {
                return false;
            }
            return true;
        }

        /*
         * Gets the name of the class the instance provides information for
         *
         * @return The class name
         */
        public Class<?> getType() {
            return type;
        }

        public Constructor<?> getDefaultConstructor() {
            if (defaultConstructor != null) {
                return defaultConstructor;
            } else {
                throw new ReflectionException("There is no default constructor for " + type);
            }
        }

        public boolean hasDefaultConstructor() {
            return defaultConstructor != null;
        }

        public Invoker getSetInvoker(String propertyName) {
            Invoker method = setMethods.get(propertyName);
            if (method == null) {
                throw new ReflectionException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
            }
            return method;
        }

        public Invoker getGetInvoker(String propertyName) {
            Invoker method = getMethods.get(propertyName);
            if (method == null) {
                throw new ReflectionException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
            }
            return method;
        }

        /*
         * Gets the type for a property setter
         *
         * @param propertyName - the name of the property
         * @return The Class of the propery setter
         */
        public Class<?> getSetterType(String propertyName) {
            Class<?> clazz = setTypes.get(propertyName);
            if (clazz == null) {
                throw new ReflectionException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
            }
            return clazz;
        }

        /*
         * Gets the type for a property getter
         *
         * @param propertyName - the name of the property
         * @return The Class of the propery getter
         */
        public Class<?> getGetterType(String propertyName) {
            Class<?> clazz = getTypes.get(propertyName);
            if (clazz == null) {
                throw new ReflectionException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
            }
            return clazz;
        }

        /*
         * Gets an array of the readable properties for an object
         *
         * @return The array
         */
        public String[] getGetablePropertyNames() {
            return readablePropertyNames;
        }

        /*
         * Gets an array of the writeable properties for an object
         *
         * @return The array
         */
        public String[] getSetablePropertyNames() {
            return writeablePropertyNames;
        }

        /*
         * Check to see if a class has a writeable property by name
         *
         * @param propertyName - the name of the property to check
         * @return True if the object has a writeable property by the name
         */
        public boolean hasSetter(String propertyName) {
            return setMethods.keySet().contains(propertyName);
        }

        /*
         * Check to see if a class has a readable property by name
         *
         * @param propertyName - the name of the property to check
         * @return True if the object has a readable property by the name
         */
        public boolean hasGetter(String propertyName) {
            return getMethods.keySet().contains(propertyName);
        }

        public String findPropertyName(String name) {
            return caseInsensitivePropertyMap.get(name.toUpperCase(Locale.ENGLISH));
        }
    }


    public interface Invoker {
        Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException;

        Class<?> getType();
    }

    static class MethodInvoker implements Invoker {

        private final Class<?> type;
        private final Method method;

        public MethodInvoker(Method method) {
            this.method = method;

            if (method.getParameterTypes().length == 1) {
                type = method.getParameterTypes()[0];
            } else {
                type = method.getReturnType();
            }
        }

        @Override
        public Object invoke(Object target, Object[] args) throws IllegalAccessException, InvocationTargetException {
            return method.invoke(target, args);
        }

        @Override
        public Class<?> getType() {
            return type;
        }
    }

    static class SetFieldInvoker implements Invoker {
        private final Field field;

        public SetFieldInvoker(Field field) {
            this.field = field;
        }

        @Override
        public Object invoke(Object target, Object[] args) throws IllegalAccessException {
            field.set(target, args[0]);
            return null;
        }

        @Override
        public Class<?> getType() {
            return field.getType();
        }
    }

    static class GetFieldInvoker implements Invoker {
        private final Field field;

        public GetFieldInvoker(Field field) {
            this.field = field;
        }

        @Override
        public Object invoke(Object target, Object[] args) throws IllegalAccessException {
            return field.get(target);
        }

        @Override
        public Class<?> getType() {
            return field.getType();
        }
    }


    static class ReflectionException extends RuntimeException {
        public ReflectionException(String message) {
            super(message);
        }
    }


    /**
     * @author Iwao AVE!
     */
    static class TypeParameterResolver {

        /**
         * @return The field type as {@link Type}. If it has type parameters in the declaration,<br>
         * they will be resolved to the actual runtime {@link Type}s.
         */
        public static Type resolveFieldType(Field field, Type srcType) {
            Type fieldType = field.getGenericType();
            Class<?> declaringClass = field.getDeclaringClass();
            return resolveType(fieldType, srcType, declaringClass);
        }

        /**
         * @return The return type of the method as {@link Type}. If it has type parameters in the declaration,<br>
         * they will be resolved to the actual runtime {@link Type}s.
         */
        public static Type resolveReturnType(Method method, Type srcType) {
            Type returnType = method.getGenericReturnType();
            Class<?> declaringClass = method.getDeclaringClass();
            return resolveType(returnType, srcType, declaringClass);
        }

        /**
         * @return The parameter types of the method as an array of {@link Type}s. If they have type parameters in the declaration,<br>
         * they will be resolved to the actual runtime {@link Type}s.
         */
        public static Type[] resolveParamTypes(Method method, Type srcType) {
            Type[] paramTypes = method.getGenericParameterTypes();
            Class<?> declaringClass = method.getDeclaringClass();
            Type[] result = new Type[paramTypes.length];
            for (int i = 0; i < paramTypes.length; i++) {
                result[i] = resolveType(paramTypes[i], srcType, declaringClass);
            }
            return result;
        }

        private static Type resolveType(Type type, Type srcType, Class<?> declaringClass) {
            if (type instanceof TypeVariable) {
                return resolveTypeVar((TypeVariable<?>) type, srcType, declaringClass);
            } else if (type instanceof ParameterizedType) {
                return resolveParameterizedType((ParameterizedType) type, srcType, declaringClass);
            } else if (type instanceof GenericArrayType) {
                return resolveGenericArrayType((GenericArrayType) type, srcType, declaringClass);
            } else {
                return type;
            }
        }

        private static Type resolveGenericArrayType(GenericArrayType genericArrayType, Type srcType, Class<?> declaringClass) {
            Type componentType = genericArrayType.getGenericComponentType();
            Type resolvedComponentType = null;
            if (componentType instanceof TypeVariable) {
                resolvedComponentType = resolveTypeVar((TypeVariable<?>) componentType, srcType, declaringClass);
            } else if (componentType instanceof GenericArrayType) {
                resolvedComponentType = resolveGenericArrayType((GenericArrayType) componentType, srcType, declaringClass);
            } else if (componentType instanceof ParameterizedType) {
                resolvedComponentType = resolveParameterizedType((ParameterizedType) componentType, srcType, declaringClass);
            }
            if (resolvedComponentType instanceof Class) {
                return Array.newInstance((Class<?>) resolvedComponentType, 0).getClass();
            } else {
                return new GenericArrayTypeImpl(resolvedComponentType);
            }
        }

        private static ParameterizedType resolveParameterizedType(ParameterizedType parameterizedType, Type srcType, Class<?> declaringClass) {
            Class<?> rawType = (Class<?>) parameterizedType.getRawType();
            Type[] typeArgs = parameterizedType.getActualTypeArguments();
            Type[] args = new Type[typeArgs.length];
            for (int i = 0; i < typeArgs.length; i++) {
                if (typeArgs[i] instanceof TypeVariable) {
                    args[i] = resolveTypeVar((TypeVariable<?>) typeArgs[i], srcType, declaringClass);
                } else if (typeArgs[i] instanceof ParameterizedType) {
                    args[i] = resolveParameterizedType((ParameterizedType) typeArgs[i], srcType, declaringClass);
                } else if (typeArgs[i] instanceof WildcardType) {
                    args[i] = resolveWildcardType((WildcardType) typeArgs[i], srcType, declaringClass);
                } else {
                    args[i] = typeArgs[i];
                }
            }
            return new ParameterizedTypeImpl(rawType, null, args);
        }

        private static Type resolveWildcardType(WildcardType wildcardType, Type srcType, Class<?> declaringClass) {
            Type[] lowerBounds = resolveWildcardTypeBounds(wildcardType.getLowerBounds(), srcType, declaringClass);
            Type[] upperBounds = resolveWildcardTypeBounds(wildcardType.getUpperBounds(), srcType, declaringClass);
            return new WildcardTypeImpl(lowerBounds, upperBounds);
        }

        private static Type[] resolveWildcardTypeBounds(Type[] bounds, Type srcType, Class<?> declaringClass) {
            Type[] result = new Type[bounds.length];
            for (int i = 0; i < bounds.length; i++) {
                if (bounds[i] instanceof TypeVariable) {
                    result[i] = resolveTypeVar((TypeVariable<?>) bounds[i], srcType, declaringClass);
                } else if (bounds[i] instanceof ParameterizedType) {
                    result[i] = resolveParameterizedType((ParameterizedType) bounds[i], srcType, declaringClass);
                } else if (bounds[i] instanceof WildcardType) {
                    result[i] = resolveWildcardType((WildcardType) bounds[i], srcType, declaringClass);
                } else {
                    result[i] = bounds[i];
                }
            }
            return result;
        }

        private static Type resolveTypeVar(TypeVariable<?> typeVar, Type srcType, Class<?> declaringClass) {
            Type result = null;
            Class<?> clazz = null;
            if (srcType instanceof Class) {
                clazz = (Class<?>) srcType;
            } else if (srcType instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) srcType;
                clazz = (Class<?>) parameterizedType.getRawType();
            } else {
                throw new IllegalArgumentException("The 2nd arg must be Class or ParameterizedType, but was: " + srcType.getClass());
            }

            if (clazz == declaringClass) {
                Type[] bounds = typeVar.getBounds();
                if (bounds.length > 0) {
                    return bounds[0];
                }
                return Object.class;
            }

            Type superclass = clazz.getGenericSuperclass();
            result = scanSuperTypes(typeVar, srcType, declaringClass, clazz, superclass);
            if (result != null) {
                return result;
            }

            Type[] superInterfaces = clazz.getGenericInterfaces();
            for (Type superInterface : superInterfaces) {
                result = scanSuperTypes(typeVar, srcType, declaringClass, clazz, superInterface);
                if (result != null) {
                    return result;
                }
            }
            return Object.class;
        }

        private static Type scanSuperTypes(TypeVariable<?> typeVar, Type srcType, Class<?> declaringClass, Class<?> clazz, Type superclass) {
            Type result = null;
            if (superclass instanceof ParameterizedType) {
                ParameterizedType parentAsType = (ParameterizedType) superclass;
                Class<?> parentAsClass = (Class<?>) parentAsType.getRawType();
                if (declaringClass == parentAsClass) {
                    Type[] typeArgs = parentAsType.getActualTypeArguments();
                    TypeVariable<?>[] declaredTypeVars = declaringClass.getTypeParameters();
                    for (int i = 0; i < declaredTypeVars.length; i++) {
                        if (declaredTypeVars[i] == typeVar) {
                            if (typeArgs[i] instanceof TypeVariable) {
                                TypeVariable<?>[] typeParams = clazz.getTypeParameters();
                                for (int j = 0; j < typeParams.length; j++) {
                                    if (typeParams[j] == typeArgs[i]) {
                                        if (srcType instanceof ParameterizedType) {
                                            result = ((ParameterizedType) srcType).getActualTypeArguments()[j];
                                        }
                                        break;
                                    }
                                }
                            } else {
                                result = typeArgs[i];
                            }
                        }
                    }
                } else if (declaringClass.isAssignableFrom(parentAsClass)) {
                    result = resolveTypeVar(typeVar, parentAsType, declaringClass);
                }
            } else if (superclass instanceof Class) {
                if (declaringClass.isAssignableFrom((Class<?>) superclass)) {
                    result = resolveTypeVar(typeVar, superclass, declaringClass);
                }
            }
            return result;
        }

        private TypeParameterResolver() {
            super();
        }

        static class ParameterizedTypeImpl implements ParameterizedType {
            private Class<?> rawType;

            private Type ownerType;

            private Type[] actualTypeArguments;

            public ParameterizedTypeImpl(Class<?> rawType, Type ownerType, Type[] actualTypeArguments) {
                super();
                this.rawType = rawType;
                this.ownerType = ownerType;
                this.actualTypeArguments = actualTypeArguments;
            }

            @Override
            public Type[] getActualTypeArguments() {
                return actualTypeArguments;
            }

            @Override
            public Type getOwnerType() {
                return ownerType;
            }

            @Override
            public Type getRawType() {
                return rawType;
            }

            @Override
            public String toString() {
                return "ParameterizedTypeImpl [rawType=" + rawType + ", ownerType=" + ownerType + ", actualTypeArguments=" + Arrays.toString(actualTypeArguments) + "]";
            }
        }

        static class WildcardTypeImpl implements WildcardType {
            private Type[] lowerBounds;

            private Type[] upperBounds;

            private WildcardTypeImpl(Type[] lowerBounds, Type[] upperBounds) {
                super();
                this.lowerBounds = lowerBounds;
                this.upperBounds = upperBounds;
            }

            @Override
            public Type[] getLowerBounds() {
                return lowerBounds;
            }

            @Override
            public Type[] getUpperBounds() {
                return upperBounds;
            }
        }

        static class GenericArrayTypeImpl implements GenericArrayType {
            private Type genericComponentType;

            private GenericArrayTypeImpl(Type genericComponentType) {
                super();
                this.genericComponentType = genericComponentType;
            }

            @Override
            public Type getGenericComponentType() {
                return genericComponentType;
            }
        }
    }


    static final class PropertyNamer {

        private PropertyNamer() {
            // Prevent Instantiation of Static Class
        }

        public static String methodToProperty(String name) {
            if (name.startsWith("is")) {
                name = name.substring(2);
            } else if (name.startsWith("get") || name.startsWith("set")) {
                name = name.substring(3);
            } else {
                throw new ReflectionException("Error parsing property name '" + name + "'.  Didn't start with 'is', 'get' or 'set'.");
            }

            if (name.length() == 1 || (name.length() > 1 && !Character.isUpperCase(name.charAt(1)))) {
                name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
            }

            return name;
        }

        public static boolean isProperty(String name) {
            return name.startsWith("get") || name.startsWith("set") || name.startsWith("is");
        }

        public static boolean isGetter(String name) {
            return name.startsWith("get") || name.startsWith("is");
        }

        public static boolean isSetter(String name) {
            return name.startsWith("set");
        }

    }


}
