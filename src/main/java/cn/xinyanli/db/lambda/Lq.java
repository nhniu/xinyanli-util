
package cn.xinyanli.db.lambda;


import cn.hutool.core.bean.BeanDesc;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.xinyanli.core.Beans;
import cn.xinyanli.core.util.ColUtil;
import cn.xinyanli.db.JdbcUtil;
import cn.xinyanli.db.annotation.TableInfo;
import cn.xinyanli.db.enums.EqType;
import cn.xinyanli.db.lambda.support.SFunction;
import cn.xinyanli.db.lambda.support.SerializedLambda;
import cn.xinyanli.db.sql.SqlUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cn.xinyanli.core.AssertConst.NOT_EMPTY;

/**
 * Lambda 语法使用 Wrapper
 *
 * @author hubin miemie HCL
 * @since 2017-05-26
 */
@Slf4j
@Getter
public class Lq<T> implements Serializable {

    JdbcUtil db = Beans.get(JdbcUtil.class);

    String implClassName = null;

    StringBuilder queryColum;
    String tableName;

    Class tableClass;

    List<Item> whereItems;
    List<Item> setItems;
    List<OrderItem> orderItems;

    private Integer page;
    private Integer size;

    /**
     * 不执行查询
     */
    private boolean notExcute;

    private Lq() {
        queryColum = new StringBuilder();
        whereItems = new ArrayList<>();
        setItems = new ArrayList<>();
        orderItems = new ArrayList<>();
    }

    @AllArgsConstructor
    @Data
    public static class OrderItem implements Serializable {
        String name;
        boolean asc;
    }

    @AllArgsConstructor
    @Data
    public static class Item implements Serializable {
        String name;
        Object val;

        EqType eqType;
        /**
         * 是否是原生sql
         */
        boolean ogsql;
    }


    public static <T> Lq<T> n(Class<T> clazz) {

        Lq<T> tLq = new Lq<>();

        TableInfo annotation = clazz.getAnnotation(TableInfo.class);
        tLq.tableName = annotation == null ? clazz.getSimpleName().toLowerCase() : annotation.value();
        tLq.tableClass = clazz;

        tLq.buildColumnName(clazz);
        return tLq;
    }


    public static <T> Lq<T> select(SFunction<T, ?>... column) {

        Lq<T> tLq = new Lq<>();

        for (SFunction<T, ?> tsFunction : column) {
            SerializedLambda resolve = LambdaUtils.resolve(tsFunction);

            if (tLq.tableClass == null) {
                tLq.calcTableClass(resolve);
            }

            String field = StrUtil.getGeneralField(resolve.getImplMethodName());
            String dbField = StrUtil.toUnderlineCase(field);

            wrapColumn(tLq.queryColum, dbField).append(",");
        }
        if (tLq.queryColum.length() > 0) {
            tLq.queryColum.setLength(tLq.queryColum.length() - 1);
        }
        return tLq;
    }


    public static <T> Lq<T> n() {
        return new Lq<T>();
    }


    public <T> Lq<T> page(int page, int size) {
        Assert.isTrue(page > 0, "page need > 0");
        Assert.notNull(size > 0, "size need > 0");
        this.page = page;
        this.size = size;
        return (Lq<T>) this;
    }

    /**
     * 原生sql
     *
     * @param s
     * @param <T>
     * @return
     */
    public <T> Lq<T> eq(String s) {
        whereItems.add(new Item(null, s, EqType.EQ, true));
        return (Lq<T>) this;
    }

    public <T> Lq<T> ne(SFunction<T, ?> column, Object val) {
        return eq0(column, val, EqType.NE);
    }

    public <T> Lq<T> eq(SFunction<T, ?> column, Object val) {
        return eq0(column, val, EqType.EQ);
    }

    private <T> Lq<T> eq0(SFunction<T, ?> column, Object val, EqType eqType) {
        SerializedLambda resolve = LambdaUtils.resolve(column);

        validColumn(resolve);

        String field = StrUtil.getGeneralField(resolve.getImplMethodName());
        String dbField = StrUtil.toUnderlineCase(field);

        whereItems.add(new Item(dbField, val, eqType, false));

        return (Lq<T>) this;
    }

    public <T, P, K> Lq<T> in(SFunction<T, ?> column, Collection<P> vals, Function<P, K> prop) {
        List<K> ks = ColUtil.list2Props(vals, prop);
        return in0(column, ks, EqType.IN);
    }

    public <T, K> Lq<T> notIn(SFunction<T, ?> column, Collection<K> vals) {
        return in0(column, vals, EqType.NOT_IN);
    }

    public <T, K> Lq<T> in(SFunction<T, ?> column, Collection<K> vals) {
        return in0(column, vals, EqType.IN);
    }

    public <T, K> Lq<T> in0(SFunction<T, ?> column, Collection<K> vals, EqType eqType) {
        Collection<K> vals1 = new LinkedHashSet(vals);
        SerializedLambda resolve = LambdaUtils.resolve(column);
        validColumn(resolve);
        if (CollUtil.isEmpty(vals1)) {
            log.info("vals is empty");
            // in如果是空 不执行查询
            notExcute = true;
            return (Lq<T>) this;
        }
        String field = StrUtil.getGeneralField(resolve.getImplMethodName());
        String dbField = StrUtil.toUnderlineCase(field);

        String inStr = vals1.stream().map(v -> SqlUtil.wrapObj(v)).collect(Collectors.joining(","));
        inStr = dbField + " " + eqType.getText().replaceAll("\\?", inStr) + " ";
        whereItems.add(new Item(dbField, inStr, EqType.IN, true));

        return (Lq<T>) this;
    }


    public <T> Lq<T> order(SFunction<T, ?> column, boolean isAsc) {
        SerializedLambda resolve = LambdaUtils.resolve(column);
        validColumn(resolve);

        String field = StrUtil.getGeneralField(resolve.getImplMethodName());
        String dbField = StrUtil.toUnderlineCase(field);

        orderItems.add(new OrderItem(dbField, isAsc));
        return (Lq<T>) this;
    }

    public <T> Lq<T> set(SFunction<T, ?> column, Object val) {
        SerializedLambda resolve = LambdaUtils.resolve(column);
        validColumn(resolve);

        String field = StrUtil.getGeneralField(resolve.getImplMethodName());
        String dbField = StrUtil.toUnderlineCase(field);

        setItems.add(new Item(dbField, val, EqType.EQ, false));

        return (Lq<T>) this;
    }

    /**
     * 原生sql
     *
     * @param s
     * @param <T>
     * @return
     */
    public <T> Lq<T> set(String s) {
        setItems.add(new Item(null, s, EqType.EQ, true));
        return (Lq<T>) this;
    }

    public String getSelect() {
        Assert.notEmpty(tableName, NOT_EMPTY, "tableName");
        StringBuilder sql = new StringBuilder();
        sql.append("select ").append(queryColum).append(" from ").append(tableName);

        appendWhereItems(sql);

        return sql.toString();
    }


    public String getCount() {
        Assert.notEmpty(tableName);
        StringBuilder sql = new StringBuilder();
        sql.append("select count(1) from ").append(tableName);

        appendWhereItems(sql);

        return sql.toString();
    }

    /**
     * 得到更新语句
     *
     * @return
     */
    public String getUpdate() {
        Assert.notEmpty(tableName);

        StringBuilder sql = new StringBuilder();
        sql.append("update ").append(tableName + " set ");

        for (int i = 0; i < setItems.size(); i++) {
            Item item = setItems.get(i);
            if (item.ogsql) {
                sql.append(item.val);
            } else {
                wrapColumn(sql, item.name).append("=?");
            }

            if (i != setItems.size() - 1) {
                sql.append(",");
            }
        }
        appendWhereItems(sql);

        return sql.toString();
    }


    public String getDelete() {
        Assert.notEmpty(tableName);

        StringBuilder sql = new StringBuilder();
        sql.append("delete from ").append(tableName);

        appendWhereItems(sql);

        return sql.toString();
    }

    public Object[] getSelectParam() {
        Object[] wheres = whereItems.stream().filter(v -> !v.ogsql).map(v -> v.val).toArray(Object[]::new);
        return wheres;
    }

    /**
     * 参数值
     *
     * @return
     */
    public Object[] getUpdateParam() {
        Object[] ups = setItems.stream().filter(v -> !v.ogsql).map(v -> v.val).toArray(Object[]::new);

        Object[] wheres = getSelectParam();
        return ArrayUtil.addAll(ups, wheres);
    }

    public Class<T> getTableClass() {
        return tableClass;
    }

    private static StringBuilder wrapColumn(StringBuilder sb, String column) {
        return sb.append("`").append(column).append("`");
    }

    private void appendWhereItems(StringBuilder sql) {
        for (int i = 0; i < whereItems.size(); i++) {
            Item item = whereItems.get(i);

            if (i == 0) {
                sql.append(" where ");
            }
            // 已经处理过的sql不处理
            if (item.ogsql) {
                sql.append(item.val);
            } else {
                wrapColumn(sql, item.name).append(" ").append(item.eqType.getText()).append(" ");
            }
            if (i != whereItems.size() - 1) {
                sql.append(" and ");
            }
        }

        if (!orderItems.isEmpty()) {
            sql.append(" order by ");
            for (OrderItem orderItem : orderItems) {
                wrapColumn(sql, orderItem.name).append(" ").append(orderItem.asc ? "asc" : "desc").append(",");
            }
            sql.setLength(sql.length() - 1);
        }

        if (page != null && size != null) {
            sql.append(" limit ").append((page - 1) * size).append(",").append(size);
        }
    }

    private <T> void buildColumnName(Class<T> clazz) {
        BeanDesc beanDesc = BeanUtil.getBeanDesc(clazz);
        for (BeanDesc.PropDesc prop : beanDesc.getProps()) {
            if (!SqlUtil.isSelectType(prop)) {
                continue;
            }
            wrapColumn(queryColum, StrUtil.toUnderlineCase(prop.getFieldName()));
            queryColum.append(",");
        }
        if (queryColum.length() > 0) {
            queryColum.setLength(queryColum.length() - 1);
        }
    }


    /**
     * 读取表 类
     *
     * @param resolve
     */
    private void validColumn(SerializedLambda resolve) {
        if (tableClass == null) {
            calcTableClass(resolve);
            buildColumnName(tableClass);
        }
        Assert.isTrue(tableClass.equals(resolve.getImplClass()), "不是同一个对象{}|{}",
                tableClass, resolve.getImplClassName());
    }

    private void calcTableClass(SerializedLambda resolve) {
        tableClass = resolve.getImplClass();
        tableName = SqlUtil.getTableName(tableClass);
    }

    public int update() {
        return db.update(this);
    }

    public long insert() {
        return db.insert(this);
    }
}
