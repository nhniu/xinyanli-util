package cn.xinyanli.db.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EqType {

    EQ("=?"),
    NE("!=?"),
    GT(">?"),
    GE(">=?"),
    LT("<?"),
    LE("<=?"),
    LIKE("like ?"),
    ISNULL("is null"),
    IN("in (?)"),
    NOT_IN("not in (?)");

    private String text;

}
