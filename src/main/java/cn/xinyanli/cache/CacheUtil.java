package cn.xinyanli.cache;

import cn.hutool.cache.impl.LRUCache;

import java.util.function.Supplier;

public class CacheUtil {

    static LRUCache<String, String> lruCache = cn.hutool.cache.CacheUtil.newLRUCache(300);

    public static String getLruFromCache(Object key, Supplier<String> s) {
        String content = lruCache.get(key.toString());
        if (content == null) {
            content = s.get();
            lruCache.put(key.toString(), content);
        }
        return content;
    }
}
