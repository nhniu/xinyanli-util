package cn.xinyanli.data;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;

/**
 * 随机数填充bean
 *
 * @author hewei
 * @date 2019/10/15 21:18
 **/
public class RandomFillBean {

    /**
     * 输出json
     *
     * @param clazz
     * @return
     */
    public static String getJson(Class<?> clazz) {
        Object object = getObject(clazz);
        return JSONObject.toJSONString(object);
    }

    @SneakyThrows
    public static <T> T getObject(Class<?> clazz) {
        T t = null;
        if (clazz == null) {
            return t;
        }
        // 填充的对象是不是基本类型
        if ((t = (T) getPrimitive(clazz)) != null) {
            return t;
        }
        //需要有无参的构造器
        t = (T) clazz.newInstance();
        Field[] fields = clazz.getDeclaredFields();


        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            String fileName = field.getName();
            //获取set方法对象
            Method method = clazz.getMethod("set" + fileName.substring(0, 1).toUpperCase() + fileName.substring(1), field.getType());


            Class<?> type1 = field.getType();
            if (type1 == byte.class || type1 == Byte.class) {
                method.invoke(t, SelfUtils.getByteValue());
            } else if (type1 == short.class || type1 == Short.class) {
                method.invoke(t, SelfUtils.getShortValue());
            } else if (type1 == char.class || type1 == Character.class) {
                method.invoke(t, SelfUtils.getCharValue());
            } else if (type1 == int.class || type1 == Integer.class) {

                method.invoke(t, SelfUtils.getIntValue());
            } else if (type1 == float.class || type1 == Float.class) {

                method.invoke(t, SelfUtils.getFloatValue());
            } else if (type1 == double.class || type1 == Double.class) {

                method.invoke(t, SelfUtils.getDoubleValue());
            } else if (type1 == long.class || type1 == Long.class) {

                method.invoke(t, SelfUtils.getLongValue());
            } else if (type1 == boolean.class || type1 == Boolean.class) {

                method.invoke(t, SelfUtils.getBooleanValue());
            } else if (type1 == String.class) {

                method.invoke(t, SelfUtils.getRamdomString(8));
            } else if (type1 == Date.class) {
                method.invoke(t, SelfUtils.getDateValue());
            } else if (type1 == BigDecimal.class) {
                method.invoke(t, SelfUtils.getBigDecimal());
            } else if (type1.isAssignableFrom(List.class)) {
                //获取泛型
                Type type = field.getGenericType();
                //如果不是泛型，不做处理
                if (type == null) {
                    continue;
                }
                if (type instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type;
                    Class<?> genericClazz = (Class) parameterizedType.getActualTypeArguments()[0];
                    int length = 0;
                    String listLength = "4";
                    if (StrUtil.isEmpty(listLength) || !listLength.matches(SelfUtils.allNumber)) {
                        length = 4;
                    }
                    length = Integer.parseInt(listLength);
                    List<Object> list = new ArrayList<>(length);
                    for (int j = 0; j < length; j++) {
                        list.add(getObject((Class<T>) genericClazz));
                    }
                    method.invoke(t, list);
                }
            }
            //如果是Map类型
            else if (type1.isAssignableFrom(Map.class)) {
                //获取泛型
                Type types = field.getGenericType();
                //如果不是泛型的话则不处理
                if (types == null) {
                    continue;
                }
                if (types instanceof ParameterizedType) {
                    int length = 4;
                    ParameterizedType parameterizedType = (ParameterizedType) types;
                    Map<Object, Object> map = new HashMap();
                    for (int j = 0; j < length; j++) {
                        map.put(getObject((Class<?>) parameterizedType.getActualTypeArguments()[0]),
                                getObject((Class<?>) parameterizedType.getActualTypeArguments()[1]));
                    }
                    method.invoke(t, map);
                }
            } else if (type1.isEnum()) {
                method.invoke(t, SelfUtils.getEnumValue(type1));
            }
            //这里默认处理的是自定义对象
            else {
                Object obj = getObject(type1);
                method.invoke(t, obj);
            }
        }
        return t;
    }

    private static Object getPrimitive(Class<?> clazz) {
        //如果是byte类型(包含基本类型和包装类)
        if (clazz == byte.class || clazz == Byte.class) {
            return SelfUtils.getByteValue();
        }
        //如果是short类型(包含基本类型和包装类)
        if (clazz == short.class || clazz == Short.class) {
            return SelfUtils.getShortValue();
        }
        //如果是char类型(包含基本类型和包装类)
        if (clazz == char.class || clazz == Character.class) {
            return SelfUtils.getCharValue();
        }
        //如果是整型(包含基本类型和包装类)
        if (clazz == int.class || clazz == Integer.class) {
            //为属性赋值
            return SelfUtils.getIntValue();
        }
        //如果是float(包含基本类型和包装类)
        if (clazz == float.class || clazz == Float.class) {
            //为属性赋值
            return SelfUtils.getFloatValue();
        }
        //如果是double(包含基本类型和包装类)
        if (clazz == double.class || clazz == Double.class) {
            //为属性赋值
            return SelfUtils.getDoubleValue();
        }
        //如果是double(包含基本类型和包装类)
        if (clazz == long.class || clazz == Long.class) {
            //为属性赋值
            return SelfUtils.getDoubleValue();
        }
        //如果是boolean(包含基本类型和包装类)
        if (clazz == boolean.class || clazz == Boolean.class) {
            return SelfUtils.getBooleanValue();
        }
        //如果是boolean(包含基本类型和包装类)
        if (clazz == String.class) {
            //为属性赋值
            return SelfUtils.getRamdomString(8);
        }
        //如果是日期类型
        if (clazz == Date.class) {
            return SelfUtils.getDateValue();
        }
        return null;
    }

    static class SelfUtils {
        public static final String allNumber = "[0-9]{1,}";
        private static Random random = new Random();
        private static char[] ch = {'A', 'B', 'C', 'D', 'E', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
                'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z'};

        public static String getRamdomString(int length) {
            //        char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
            //                'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
            // 'Z', 'a', 'b',
            //                'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            // 'u', 'v', 'w',
            //                'x', 'y', 'z', 'o', '1'};
            if (length <= 0) {
                length = ch.length;
            }
            char[] chars = new char[length];
            for (int i = 0; i < length; i++) {
                chars[i] = ch[random.nextInt(ch.length)];
            }
            return new String(chars);
        }

        public static int getIntValue() {
            return random.nextInt(1000);
        }

        public static byte getByteValue() {
            return (byte) (random.nextInt() & 0xEF);
        }

        public static long getLongValue() {
            return RandomUtil.randomLong(1, 1000);
        }

        public static short getShortValue() {
            return (short) (random.nextInt() & 0xEFFF);
        }

        public static float getFloatValue() {
            return random.nextFloat();
        }

        public static double getDoubleValue() {
            return random.nextDouble();
        }

        public static char getCharValue() {
            return ch[random.nextInt(ch.length)];
        }

        public static boolean getBooleanValue() {
            return random.nextInt() % 2 == 0;
        }

        public static Date getDateValue() {
            return new Date();
        }

        public static BigDecimal getBigDecimal() {
            return new BigDecimal(RandomUtil.randomInt(1, 1000));
        }

        @SneakyThrows
        public static Object getEnumValue(Class clz) {
            Method method = clz.getMethod("values");
            Object inter[] = (Object[]) method.invoke(null, (Object[]) null);
            return inter[RandomUtil.randomInt(0, inter.length)];
        }
    }
}