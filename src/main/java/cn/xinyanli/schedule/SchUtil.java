package cn.xinyanli.schedule;

import cn.hutool.core.lang.Assert;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.Scheduler;
import cn.hutool.cron.task.Task;
import cn.xinyanli.config.EnvConfig;
import cn.xinyanli.core.Beans;
import cn.xinyanli.nacos.NacosUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 定时任务工具类
 *
 * @author hewei
 * @date 2019/10/30 11:39
 **/

@Slf4j
public class SchUtil {


    public static class MyTask implements Task {
        /**
         * 是否正在执行
         */
        private boolean runing = false;
        final String taskId;
        final Task task;

        public MyTask(String taskId, Task task) {
            this.taskId = taskId;
            this.task = task;
        }

        @Override
        public void execute() {
            long t1 = System.currentTimeMillis();
            // 如果正在执行跳过
            if (runing) {
                log.info("{}-is-runing-break", taskId);
                return;
            }
            log.info("{}-start", taskId);

            try {
                runing = true;
                task.execute();
            } finally {
                runing = false;
            }

            long t2 = System.currentTimeMillis();
            log.info("{}-run-ok|{}s", taskId, (t2 - t1) / 1000);

        }
    }


    /**
     * 重新加载任务
     */
    private static void reloadTask(String taskId, String newPattern) {
        Scheduler scheduler = CronUtil.getScheduler();
        String oldPattern = scheduler.getPattern(taskId).toString();
        Task task = scheduler.getTask(taskId);
        // 相同跳过
        if (oldPattern.equals(newPattern)) {
            return;
        }
        log.info("reloadTask|{}|new={}", taskId, newPattern);
        CronUtil.remove(taskId);
        CronUtil.schedule(taskId, newPattern, task);
        log.info("reloadTask-curTask-size|{}", scheduler.size());

    }

    public static void startSch(List<MyTask> tasks) {

        EnvConfig envConfig = Beans.get(EnvConfig.class);
        for (MyTask task : tasks) {

            String pattern = envConfig.get(task.taskId);
            Assert.notEmpty(pattern, "pattern cannot be emtpy|{}", task.taskId);

            CronUtil.schedule(task.taskId, pattern, task);
        }

        // 支持秒级别定时任务
        CronUtil.setMatchSecond(true);
        CronUtil.start(true);
    }

}
