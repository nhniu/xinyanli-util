package cn.xinyanli.generat;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
enum TmplFilePath {
    MYBATIS("generat/code_mybatis"),
    PLUS("generat/code_plus"),
    SQL2CODE("generat/sql2code");

    String name;
}