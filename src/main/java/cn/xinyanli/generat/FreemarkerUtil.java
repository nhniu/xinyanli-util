package cn.xinyanli.generat;

import cn.hutool.core.io.IoUtil;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.SneakyThrows;

import java.io.InputStreamReader;
import java.io.StringWriter;

public class FreemarkerUtil {


    /**
     * 解析模板
     * 使用内部静态类对象 记得加public 不然取不到
     */
    @SneakyThrows
    public static StringWriter processTemplate(Configuration configuration, String text,Object dataModel){

        StringWriter stringWriter = new StringWriter();
        Template template = new Template("CODE", text, configuration);
        template.process(dataModel, stringWriter);
        return stringWriter;
    }

    /**
     * 获得模板字符串
     *
     * @param codeType
     * @param ftl
     * @return
     */
    @SneakyThrows
    public static String getTmplStr(TmplFilePath codeType, String ftl) {
        return IoUtil.read(new InputStreamReader(FreemarkerUtil.class.getClassLoader().getResourceAsStream(codeType.getName() + "/" + ftl)));
    }

    /**
     * 配置 freemarker configuration
     *
     * @return
     */
    public static Configuration configuration() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_27);
        StringTemplateLoader templateLoader = new StringTemplateLoader();
        configuration.setTemplateLoader(templateLoader);
        configuration.setDefaultEncoding("UTF-8");
        //null值不报错
        configuration.setClassicCompatible(true);
        return configuration;
    }
}
