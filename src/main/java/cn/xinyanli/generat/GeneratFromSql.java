package cn.xinyanli.generat;

import cn.hutool.core.util.StrUtil;
 import freemarker.template.Configuration;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static cn.xinyanli.generat.FreemarkerUtil.getTmplStr;
import static cn.xinyanli.generat.MysqlSqlUtil.sqlType2JavaType;


/**
 * 连接数据库生成po类
 *
 * @author hewei
 * @date 2019/5/7 11:50
 **/
@Slf4j
public class GeneratFromSql {


    @Test
    public void dd() {
        String s = "CREATE TABLE `t_comment` (\n" +
                "  `id` bigint(20) NOT NULL,\n" +
                "  `third_user` varchar(255) DEFAULT NULL,\n" +
                "  `user_id` bigint(20) DEFAULT NULL COMMENT '评论用户id',\n" +
                "  `to_third_user` varchar(255) DEFAULT NULL COMMENT '第三方用户名 ，链接地址',\n" +
                "  `to_user_id` bigint(20) DEFAULT NULL COMMENT '被评论的用户id',\n" +
                "  `msg` varchar(255) DEFAULT NULL,\n" +
                "  `thumbs_up` int(255) DEFAULT NULL COMMENT '点赞数',\n" +
                "  `article_id` bigint(20) DEFAULT NULL COMMENT '文章id',\n" +
                "  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,\n" +
                "  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n" +
                "  `nick` varchar(255) DEFAULT NULL,\n" +
                "  `logo` varchar(255) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`id`)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

        generalFromDml(s);
    }


    public void generalFromDml(String dml) {

        String s = dml.toLowerCase();
        String tableName = StrUtil.subBetween(s, "table", "(").replaceAll("`", "").trim();
        String javaClassName = StrUtil.upperFirst(StrUtil.toCamelCase(tableName.replaceAll("t_", "")));
        int ci = StrUtil.lastIndexOfIgnoreCase(s, "comment='");
        String tableComment = ci == -1 ? "" : StrUtil.sub(s, ci + 9, StrUtil.lastIndexOfIgnoreCase(s, "';"));

        Et et = new Et().setTableName(tableName).setTableComment(tableComment).setJavaClassName(javaClassName);
        List<Item> items = et.items;

        String contentStr = StrUtil.sub(s, s.indexOf('(') + 1, s.lastIndexOf(')'));
        for (String s1 : contentStr.split(",")) {
            if (s1.contains(" key ")) {
                continue;
            }
            s1 = s1.trim();
            String[] s2 = s1.split(" ", 3);
            String colName = s2[0].replaceAll("`", "");
            String colType = StrUtil.subBefore(s2[1], "(", false).replaceAll("`", "");
            String endStr = s2[2];
            String colComment = StrUtil.subAfter(endStr, "comment", true).replaceAll("'", "");
            if (StrUtil.isEmpty(colComment)) {
                colComment = colName;
            }
            Item item = new Item().setColName(colName).setColType(colType).setColComment(colComment)
                    .setJavaName(StrUtil.toCamelCase(colName)).setJavaType(sqlType2JavaType(colType, false));
            items.add(item);
        }

        createJavaCode(et);
    }


    @SneakyThrows
    public void createJavaCode(Et et) {
        Configuration configuration = FreemarkerUtil.configuration();
        String tmpl = getTmplStr(TmplFilePath.SQL2CODE, "sql2code.ftl");

        StringWriter swTmplMapper = FreemarkerUtil.processTemplate(configuration, tmpl, et);

        System.out.println(swTmplMapper);
    }

    @Data
    @Accessors(chain = true)
    public static class Et {
        String tableName;
        String tableComment;
        String javaClassName;

        List<Item> items = new ArrayList<>();
    }

    @Accessors(chain = true)
    @Data
    public static class Item {
        String colName;
        String colType;
        String colComment;

        String javaType;
        String javaName;
    }


}

