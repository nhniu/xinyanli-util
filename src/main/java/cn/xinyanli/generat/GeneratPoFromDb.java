package cn.xinyanli.generat;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
 import freemarker.template.Configuration;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

 import static cn.xinyanli.generat.FreemarkerUtil.getTmplStr;
import static cn.xinyanli.generat.FreemarkerUtil.processTemplate;
import static cn.xinyanli.generat.MysqlSqlUtil.sqlType2JavaType;


/**
 * 连接数据库生成po类
 *
 * @author hewei
 * @date 2019/5/7 11:50
 **/
@Slf4j
public class GeneratPoFromDb {


    private Connection getConnection = null;

    private String driver;
    private String pwd;
    private String user;
    private String dburl;
    private String dbname;


    // 生成的 表名
    private List<String> tablenames;


    private String author;
    private String classNameExt;

    /**
     * 是否写文件
     */
    private boolean writeFile = false;

    /**
     * 你的实体类所在的包的位置
     */
    private String pkg;
    private String poPackage;
    private String daoPackage;
    /**
     * 文件路径
     */
    private String filePath;
    private String busName;

    private String prefix;
    private boolean useLocalDate;


    @SneakyThrows
    public void general(GenerateParam param) {

        this.driver = param.getDriver();
        this.pwd = param.getPwd();
        this.user = param.getUser();
        this.dburl = param.getDburl();
        this.dbname = param.getDbname();
        this.tablenames = param.getTablenames();

        this.author = param.getAuthor();
        this.writeFile = param.isWriteFile();
        this.pkg = param.getCompanyPkg() + "." + param.getModuleName() +".modules";
        this.poPackage = pkg + "." + param.getPoPkgName();
        this.daoPackage = pkg + "." + param.getDaoPkgName();
        this.filePath = param.getFilePath();
        this.busName = param.getBusName();
        this.classNameExt = param.getClassNameExt();
        this.prefix = param.getPrefix();
        this.useLocalDate = param.isUseLocalDate();

        getConnection = getConnections();


        String tmplService = getTmplStr(param.getCodeType(), "Service.ftl");
        String tmplMapper = getTmplStr(param.getCodeType(), "Mapper.ftl");
        String tmplController = getTmplStr(param.getCodeType(), "Controller.ftl");
        String tmplInterface = getTmplStr(param.getCodeType(), "Interface.ftl");
        String tmplPo = getTmplStr(param.getCodeType(), "Po.ftl");
        String tmplServiceTest = getTmplStr(param.getCodeType(), "ServiceTest.ftl");
        String tmplQueryListReq = getTmplStr(param.getCodeType(), "QueryReq.ftl");
        String tmplDetailVo = getTmplStr(param.getCodeType(), "DetailVo.ftl");
        String tmplRpcService = getTmplStr(param.getCodeType(), "RpcService.ftl");
        String tmplAdminRest = getTmplStr(param.getCodeType(), "AdminRest.ftl");


        DatabaseMetaData dbmd = getConnection.getMetaData();
        // catalog为null是所有数据库
        ResultSet resultSet = dbmd.getTables(dbname, "%", "%", new String[]{"TABLE"});

        Configuration configuration = FreemarkerUtil.configuration();

        while (resultSet.next()) {
            String tableName = resultSet.getString("TABLE_NAME");
            //这里干掉IF可对库里面所有表直接生成
            if (tablenames.get(0).equals("*") || tablenames.contains(tableName)) {

                StringBuilder sb = new StringBuilder(100);

                //类名 文件名规则
                String className = Util.initcap(StrUtil.toCamelCase(tableName.replaceAll("^t_", "")));
                String slowClassName = Util.initlow(className);

                List<Prop> props = genPo(dbmd, tableName);

                String baseSql = props.stream().map(v -> v.getCol()).collect(Collectors.joining(","));
                String tableRemark = resultSet.getString("REMARKS");

                //insert
                Map<String, Object> map = new HashMap();
                map.put("tableName", tableName);
                map.put("slowClassName", slowClassName);
                map.put("className", className);
                map.put("moduleName", param.getModuleName());
                map.put("moduleNameCap", Util.initcap(param.getModuleName()));
                map.put("poName", className + param.getPoExtName());
                map.put("slowPoName", slowClassName + param.getPoExtName());
                map.put("author", author);
                map.put("pclassName", poPackage + "." + className);
                map.put("pclassNamePo", poPackage + "." + className + param.getPoExtName());
                map.put("date", DateUtil.format(new Date(), "yyyy/MM/dd HH:ss"));
                map.put("busName", busName);
                map.put("ext", classNameExt);
                map.put("slowExt", classNameExt.toLowerCase());
                map.put("poPkg", poPackage);
                map.put("daoPkg", daoPackage);
                map.put("pkg", pkg);
                map.put("baseSql", baseSql);
                map.put("cs", props);
                map.put("tableRemark", tableRemark);
                map.put("prefix", prefix);


                //mapper
                List<Prop> cs1 = props.stream().filter(f -> !("create_time".equals(f.col) || "update_time".equals(f.col))).collect(Collectors.toList());
                map.put("cs1", cs1);

                List<Prop> cs2 = props.stream().filter(f -> !("id".equals(f.col) || "create_time".equals(f.col) || "update_time".equals(f.col))).collect(Collectors.toList());
                map.put("cs2", cs2);

                // 搜索条件
                List<Prop> cs3 = props.stream().filter(f -> !("id".equals(f.col)
                        || "update_time".equals(f.col) || "create_by".equals(f.col) || "update_by".equals(f.col)
                        || f.col.contains("img") || f.col.contains("desc") || f.col.contains("old_id") || f.col.contains("info") || f.type.equals("BigDecimal")
                )).collect(Collectors.toList());

                // 替换create_time
                for (int i = 0; i < cs3.size(); i++) {
                    Prop next = cs3.get(i);
                    if (next.col.equals("create_time")) {
                        cs3.remove(i);

                        String t = next.col.split("_")[0];
                        String startCol = t + "_start";
                        String endCol = t + "_end";
                        String propstart = StrUtil.toCamelCase(startCol);
                        Prop pstart = new Prop().setProp(propstart).setCol(startCol).setOldWhereCol(next.col)
                                .setOldProp(Util.initlow(next.prop))
                                .setWhereType(2).setType("Date").setDeft("new Date()").setUpprop(Util.initcap(propstart))
                                .setDesc(next.desc + "-开始");

                        String propend = StrUtil.toCamelCase(endCol);
                        Prop pend = new Prop().setProp(propend).setCol(endCol).setOldWhereCol(next.col)
                                .setOldProp(Util.initlow(next.prop))
                                .setWhereType(3).setType("Date").setDeft("new Date()").setUpprop(Util.initcap(propend))
                                .setDesc(next.desc + "-结束");

                        cs3.add(pstart);
                        cs3.add(pend);

                        i += 1;
                    }
                }
                map.put("cs3", cs3);

                // 只有update_by
                List<Prop> cs4 = props.stream().filter(f -> !("id".equals(f.col) || "create_time".equals(f.col)
                        || "update_time".equals(f.col) || "create_by".equals(f.col))).collect(Collectors.toList());
                map.put("cs4", cs4);


                StringWriter swTmplPo = processTemplate(configuration, tmplPo, map);
                sb.append("\n-----" + className + "Po-----").append("\n").append(swTmplPo);

                StringWriter swTmplQueryListReq = processTemplate(configuration, tmplQueryListReq, map);
                sb.append("\n-----tmplQueryListReq-----").append("\n").append(swTmplQueryListReq);

                StringWriter swTmplDetailVo = processTemplate(configuration, tmplDetailVo, map);
                sb.append("\n-----tmplDetailVo-----").append("\n").append(swTmplDetailVo);

                StringWriter swTmplMapper = processTemplate(configuration, tmplMapper, map);
                sb.append("\n-----" + className + "Mapper.xml-----").append("\n").append(swTmplMapper);

                StringWriter swTmplInterface = processTemplate(configuration, tmplInterface, map);
                sb.append("\n-----tmplInterface-----").append("\n").append(swTmplInterface);

                StringWriter swTmplService = processTemplate(configuration, tmplService, map);
                sb.append("\n-----tmplService-----").append("\n").append(swTmplService);

                StringWriter swTmplController = processTemplate(configuration, tmplController, map);
                sb.append("\n-----tmplController-----").append("\n").append(swTmplController);

                StringWriter swTmplServiceTest = processTemplate(configuration, tmplServiceTest, map);
                sb.append("\n-----tmplServiceTest-----").append("\n").append(swTmplServiceTest);

                StringWriter swTmplRpcService = processTemplate(configuration, tmplRpcService, map);
                sb.append("\n-----tmplRpcService-----").append("\n").append(swTmplRpcService);

                StringWriter swTmplAdminRest = processTemplate(configuration, tmplAdminRest, map);
                sb.append("\n-----tmplAdminRest-----").append("\n").append(swTmplAdminRest);


                log.info(sb.toString());

                if (writeFile) {
                    writeFile(tableName, className, sb);
                }

            }
        }

    }



    /**
     * 写文件
     */
    @SneakyThrows
    public void writeFile(String tableName, String className, StringBuilder sb) {
        File directory = new File(filePath + "\\" + className + ".java");
        FileWriter fw = new FileWriter(directory);
        PrintWriter pw = new PrintWriter(fw);
        if (directory.exists()) {
        } else {
            directory.createNewFile();
        }
        pw.write(sb.toString());
        pw.flush();
        pw.close();

        log.info("write file succ " + tableName);
    }


    @Data
    @Accessors(chain = true)
    public static class Prop {
        String col;

        /**
         * 小写开头
         */
        String prop;

        /**
         * 大写开头
         */
        String upprop;

        String type;

        String desc;

        /**
         * 默认值
         */
        String deft;

        /**
         * 1使用concat比较
         * 2比较时间 >=
         * 3比较时间 <=
         */
        int whereType;

        String oldWhereCol;
        String oldProp;

        /**
         * 是否允许为空
         */
        boolean notNull;

    }

    /**
     * 特殊字符
     */
    List<String> mysqlSpec = Arrays.asList("desc", "name");

    @SneakyThrows
    public List<Prop> genPo(DatabaseMetaData dbmd, String tableName) {

        //ResultSet rs =getConnection.getMetaData().getColumns(null, getXMLConfig.getSchema(),tableName.toUpperCase(), "%");//其他数据库不需要这个方法的，直接传null，这个是oracle和db2这么用
        ResultSet rs1 = dbmd.getColumns(dbname, "%", tableName, "%");


        List<Prop> list = new ArrayList<>();
        while (rs1.next()) {

            boolean notNull = "NO".equals(rs1.getString("IS_NULLABLE"));
            String type = sqlType2JavaType(rs1.getString("TYPE_NAME"),useLocalDate);
            String col = rs1.getString("COLUMN_NAME");
            String prop = StrUtil.toCamelCase(rs1.getString("COLUMN_NAME"));
            String desc = rs1.getString("REMARKS");

            if (type == null) {
                System.out.println(rs1);
            }

            String deft = "";
            if (type.equals("String")) {
                deft = "\"xxxx\"";
            } else if (type.equals("Boolean")) {
                deft = "false";
            } else if (type.equals("Integer")) {
                deft = "100";
            } else if (type.equals("Long")) {
                deft = "100L";
            } else if (type.equals("BigDecimal")) {
                deft = "BigDecimal.TEN";
            } else if (type.equals("Date")) {
                deft = "new Date()";
            } else if (type.equals("LocalDateTime")) {
                deft = "LocalDateTime.now()";
            }

            // 特殊字符
            col = mysqlSpec.contains(col) ? "`" + col + "`" : col;
            int whereType = 0;
            if (col.contains("name") || col.contains("mobile")) {
                whereType = 1;
            }


            list.add(new Prop().setCol(col).setProp(prop).setType(type).setDesc(desc)
                    .setUpprop(Util.initcap(prop)).setDeft(deft).setWhereType(whereType).setNotNull(notNull));
        }


        return list;
    }


    // 创建数据库连接
    private Connection getConnections() {
        try {
            Class.forName(driver);

            Properties props = new Properties();

            props.setProperty("user", user);
            props.setProperty("password", pwd);

            //获取表的COMMENT信息 需要设置这个属性 否则获取不到
            props.setProperty("useInformationSchema", "true");


            String url = "jdbc:mysql://" + dburl + "/" + dbname + "?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&autoReconnect=true&useSSL=false&useServerPrepStmts=false&rewriteBatchedStatements=true&serverTimezone=GMT";
            log.info("getConnections-url|{}", url);
            getConnection = DriverManager.getConnection(url, props);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getConnection;
    }



    // 获取格式化后的时间
    private String getDate() {
        String time = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        time = sdf.format(new Date());
        return time;
    }

    static class Util {
        // 将单词字母首字母改为大写
        private static String initcap(String str) {
            char[] ch = str.toCharArray();
            if (ch[0] >= 'a' && ch[0] <= 'z') {
                ch[0] = (char) (ch[0] - 32);
            }
            return new String(ch);
        }

        // 将单词字母首字母改为小写
        private static String initlow(String str) {
            char[] ch = str.toCharArray();
            if (ch[0] >= 'A' && ch[0] <= 'Z') {
                ch[0] = (char) (ch[0] + 32);
            }
            return new String(ch);
        }
    }

}

