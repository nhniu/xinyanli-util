package cn.xinyanli.generat;

/**
 * mysql的工具类
 * @author hewei
 * @date 2019/11/17 18:18
 */
public class MysqlSqlUtil {


    /**
     * 数据库类型 转 java类型
     *
     * @param sqlType
     * @return
     */
    public static String sqlType2JavaType(String sqlType,boolean useLocalDate) {
        String str = "String";
        if (sqlType.equalsIgnoreCase("bit")) {
            str = "Boolean";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            str = "Byte";
        } else if (sqlType.equalsIgnoreCase("smallint")) {
            str = "Short";
        } else if (sqlType.equalsIgnoreCase("int")) {
            str = "Integer";
        } else if (sqlType.equalsIgnoreCase("bigint")) {
            str = "Long";
        } else if (sqlType.equalsIgnoreCase("float")) {
            str = "Float";
        } else if (sqlType.equalsIgnoreCase("decimal") || sqlType.equalsIgnoreCase("numeric")
                || sqlType.equalsIgnoreCase("real") || sqlType.equalsIgnoreCase("money")
                || sqlType.equalsIgnoreCase("smallmoney")) {
            str = "BigDecimal";
        } else if (sqlType.equalsIgnoreCase("varchar") || sqlType.equalsIgnoreCase("char")
                || sqlType.equalsIgnoreCase("nvarchar") || sqlType.equalsIgnoreCase("nchar")
                || sqlType.equalsIgnoreCase("text") || sqlType.equalsIgnoreCase("mediumtext")) {
            str = "String";
        } else if (sqlType.equalsIgnoreCase("datetime")) {

            if (useLocalDate) {
                str = "LocalDateTime";
            } else {
                str = "Date";
            }

        } else if (sqlType.equalsIgnoreCase("image")) {
            str = "Blod";
        } else if (sqlType.equalsIgnoreCase("double")) {
            str = "Double";
        } else if (sqlType.equalsIgnoreCase("time")) {
            str = "Time";
        } else if (sqlType.equalsIgnoreCase("json")) {
            str = "String";
        }
        return str;
    }

}
