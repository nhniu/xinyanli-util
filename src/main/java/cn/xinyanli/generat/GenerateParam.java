package cn.xinyanli.generat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Arrays;
import java.util.List;

/**
 * 生成PO请求参数
 * @author hewei
 * @date 2019/7/26 12:54
 **/
@Data
@Accessors(chain = true)
public class GenerateParam {




    private String driver = "com.mysql.cj.jdbc.Driver";
    private String pwd = "root";
    private String user = "root";
    private String dburl = "127.0.0.1:3306";
    private String dbname = "sjmm-mdbiz";

    private String companyPkg = "com.sjmama";
    private String moduleName = "order";
    private String poPkgName = "entity";
    private String daoPkgName = "mapper";

    // 生成的 表名
    private List<String> tablenames = Arrays.asList("t_transfer_log");

    /**
     * 业务名称
     */
    private String busName = "";

    /**
     * 类附加名字
     */
    private String classNameExt = "";


    private String author = "hewei";

    /**
     * 是否写文件
     */
    private boolean writeFile = false;

    /**
     * 文件路径
     */
    private String filePath = "D:\\po";

    /**
     * 生成额外basesql
     */
    private String prefix = "s";

    /**
     * 可以在实体类后缀加Po 默认不加
     */
    private String poExtName = "";

    private TmplFilePath codeType = TmplFilePath.PLUS;

    private boolean useLocalDate = true;
}
