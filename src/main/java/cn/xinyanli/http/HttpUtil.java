package cn.xinyanli.http;


import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.net.ssl.*;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * http、https 请求工具类， 微信为https的请求
 *
 * @author hewei
 */
public class HttpUtil {

    private static final String DEFAULT_CHARSET = "UTF-8";

    private static final String _GET = "GET";
    private static final String _POST = "POST";
    public static final int DEF_CONN_TIMEOUT = 30000;
    /**
     * 读取超时时间
     */
    public static final int DEF_READ_TIMEOUT = 300_000;

    /**
     * 初始化http请求参数
     *
     * @param url
     * @param method
     * @param headers
     * @return
     * @throws Exception
     */
    private static HttpURLConnection initHttp(String url, String method,
                                              Map<String, String> headers, Proxy proxy) throws Exception {
        URL _url = new URL(url);

        HttpURLConnection http = null;
        if (proxy != null) {
            http = (HttpURLConnection) _url.openConnection(proxy);
        } else {
            http = (HttpURLConnection) _url.openConnection();
        }
        // 连接超时
        http.setConnectTimeout(DEF_CONN_TIMEOUT);
        // 读取超时 --服务器响应比较慢，增大时间
        http.setReadTimeout(DEF_READ_TIMEOUT);
        http.setUseCaches(false);
        http.setRequestMethod(method);
        http.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        http.setRequestProperty(
                "User-Agent",
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
        if (null != headers && !headers.isEmpty()) {
            for (Entry<String, String> entry : headers.entrySet()) {
                http.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        if(_POST.equals(method)) {
            //android里面如果为true会变成post请求
            http.setDoOutput(true);
        }else{
            http.setDoOutput(false);
        }
        http.setDoInput(true);
        http.connect();
        return http;
    }

    /**
     * 初始化http请求参数
     *
     * @param url
     * @param method
     * @return
     * @throws Exception
     */
    private static HttpsURLConnection initHttps(String url, String method,
                                                Map<String, String> headers, Proxy proxy) throws Exception {
        TrustManager[] tm = {new MyX509TrustManager()};
        System.setProperty("https.protocols", "TLSv1");
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, tm, new java.security.SecureRandom());
        // 从上述SSLContext对象中得到SSLSocketFactory对象
        SSLSocketFactory ssf = sslContext.getSocketFactory();
        URL _url = new URL(url);


        HttpsURLConnection http = null;
        if (proxy != null) {
            http = (HttpsURLConnection) _url.openConnection(proxy);

        } else {
            http = (HttpsURLConnection) _url.openConnection();

        }
        // 设置域名校验
        http.setHostnameVerifier(new TrustAnyHostnameVerifier());
        // 连接超时
        http.setConnectTimeout(DEF_CONN_TIMEOUT);
        // 读取超时 --服务器响应比较慢，增大时间
        http.setReadTimeout(DEF_READ_TIMEOUT);
        http.setUseCaches(false);
        http.setRequestMethod(method);
        http.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        http.setRequestProperty(
                "User-Agent",
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
        if (null != headers && !headers.isEmpty()) {
            for (Entry<String, String> entry : headers.entrySet()) {
                http.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }
        http.setSSLSocketFactory(ssf);

        if(_POST.equals(method)) {
            //android里面如果为true会变成post请求
            http.setDoOutput(true);
        }else{
            http.setDoOutput(false);
        }
        http.setDoInput(true);
        http.connect();
        return http;
    }

    @Data
    @Accessors(chain = true)
    static class Resp{
        String content;
        Map<String, List<String>> header;
    }

    /**
     * @return 返回类型:
     * @throws Exception
     * @description 功能描述: get 请求
     */
    public static String get(String url, Map<String, Object> params, Map<String, String> headers, Proxy proxy) {
        return getResp(url, params, headers, proxy).getContent();
    }

    /**
     * @return 返回类型:
     * @throws Exception
     * @description 功能描述: get 请求
     */
    public static Resp getResp(String url, Map<String, Object> params, Map<String, String> headers, Proxy proxy) {
        try {
            HttpURLConnection http = null;
            if (isHttps(url)) {
                http = initHttps(initParams(url, params), _GET, headers, proxy);
            } else {
                http = initHttp(initParams(url, params), _GET, headers, proxy);
            }
            InputStream in = http.getInputStream();

            byte[] bytes = new byte[1024];
            //200kb
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*20);
            int len = 0;
            while ((len =in.read(bytes)) != -1) {
                bos.write(bytes, 0, len);

            }

            Map<String, List<String>> headerFields = http.getHeaderFields();

            in.close();
            if (http != null) {
                http.disconnect();// 关闭连接
            }

            //转码
            byte[] topbyte = bos.toByteArray();
            String contentType = http.getHeaderField("content-Type");
            String charset = getCharsetFromContentType(contentType);
            charset = isEmpty(charset)?"utf-8":charset;

            String s = bos.toString(charset);
            return new Resp().setContent(s).setHeader(headerFields);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String getCharsetFromContentType(String contentType) {
        String[] split = contentType.split(",");
        String charset = null;
        for (String s : split) {
            if(s != null && s.toLowerCase().contains("charset")){
                charset = StrUtil.subAfter(contentType, "charset=",false);
            }
        }
        return charset;
    }

    public static String get(String url) {
        return get(url, null);
    }

    public static String get(String url, Map<String, Object> params) {
        return get(url, params, null, null);
    }

    public static Resp getResp(String url) {
        return getResp(url, null);
    }

    public static Resp getResp(String url, Map<String, Object> params) {
        return getResp(url, params, null, null);
    }

    public static String post(String url, String params) {
        return post(url, params, null, null);
    }

    /**
     * 设置header为json
     * @param url
     * @param params
     * @return
     */
    public static String jsonPost(String url, String params) {
        Map headMap = new HashMap();
        headMap.put("Content-Type", "application/json");
        return post(url, params, headMap, null);
    }

    public static String post(String url, String params, Map<String, String> headers, Proxy proxy) {
        try {
            HttpURLConnection http = null;
            if (isHttps(url)) {
                http = initHttps(url, _POST, headers, proxy);
            } else {
                http = initHttp(url, _POST, headers, proxy);
            }
            OutputStream out = http.getOutputStream();
            if(params!=null) {
                out.write(params.getBytes(DEFAULT_CHARSET));
            }
            out.flush();
            out.close();

            InputStream in = http.getInputStream();

            byte[] bytes = new byte[1024];
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1024*20);
            int len = 0;
            while ((len =in.read(bytes)) != -1) {
                bos.write(bytes, 0, len);
            }

            in.close();
            if (http != null) {
                http.disconnect();// 关闭连接
            }
            return bos.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 功能描述: 构造请求参数
     *
     * @return 返回类型:
     * @throws Exception
     */
    public static String initParams(String url, Map<String, Object> params)
            throws Exception {
        if (null == params || params.isEmpty()) {
            return url;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.indexOf("?") == -1) {
            sb.append("?");
        }
        sb.append(map2Url(params));
        return sb.toString();
    }

    /**
     * map构造url
     *
     * @return 返回类型:
     * @throws Exception
     */
    public static String map2Url(Map<String, Object> paramToMap) {
        try {
            if (null == paramToMap || paramToMap.isEmpty()) {
                return null;
            }
            StringBuffer url = new StringBuffer();
            boolean isfist = true;
            for (Entry<String, Object> entry : paramToMap.entrySet()) {
                if (isfist) {
                    isfist = false;
                } else {
                    url.append("&");
                }
                url.append(entry.getKey()).append("=");
                String value = entry.getValue() == null?null:entry.getValue().toString();
                if (value != null && value.length() > 0) {
                    url.append(URLEncoder.encode(value, DEFAULT_CHARSET));
                }
            }
            return url.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 检测是否https
     *
     * @param url
     */
    private static boolean isHttps(String url) {
        return url.startsWith("https");
    }

    /**
     * https 域名校验
     *
     * @return
     */
    public static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;// 直接返回true
        }
    }

    static class MyX509TrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
                throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

    }

    private static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }


}