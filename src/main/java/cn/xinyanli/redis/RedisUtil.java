package cn.xinyanli.redis;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import cn.xinyanli.core.util.ObjUtil;
import lombok.Data;
import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InvocationHandler;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author tengfei
 * @version 1.0
 * @date 2018/7/13 下午4:15
 */
public class RedisUtil {

    private JedisPool pool = null;


    @Data
    public static class Config {
        String host;
        Integer port;
        String password;
        Integer maxTotal;
        Integer maxIdle;
        Long maxWaitMillis;

    }

    Config config;

    public static Jedis create(Config config) {
        // 初始化redis
        RedisUtil obj = new RedisUtil(config);
        Jedis jedis = obj.createJedisProxy();
        return jedis;
    }

    private RedisUtil(Config config) {
        if (pool == null) {
            this.config = config;
            String host = ObjUtil.getRequired(config.host,"host");
            int port = ObjUtil.getRequired(config.port,"port");

            JedisPoolConfig poolCfg = new JedisPoolConfig();
            poolCfg.setMaxTotal(ObjUtil.getVal(config.maxTotal, 1));
            poolCfg.setMaxIdle(ObjUtil.getVal(config.maxIdle, 1));
            poolCfg.setMaxWaitMillis(ObjUtil.getVal(config.maxWaitMillis, 3000L));

            if (StrUtil.isNotEmpty(config.password)) {
                // redis 设置了密码
                pool = new JedisPool(poolCfg, host, port, 10000, config.password);
            } else {
                // redis 未设置密码
                pool = new JedisPool(poolCfg, host, port, 10000);
            }
        }
    }


    /**
     * 使用cglib的动态代理 自动关闭连接
     *
     * @return
     */
    private Jedis createJedisProxy() {
        Enhancer enhancer = new Enhancer();
        //设置代理的父类，就设置需要代理的类
        enhancer.setSuperclass(Jedis.class);
        //设置自定义的代理方法
        Callback callback = (InvocationHandler) (o, method, objects) -> {
            Jedis jedis = null;
            try {
                jedis = pool.getResource();
                Object invoke = method.invoke(jedis, objects);
                return invoke;
            } finally {
                if (jedis != null) {
                    jedis.close();
                }
            }
        };
        enhancer.setCallback(callback);

        Object o = enhancer.create();
        Jedis jedis = null;
        if (o instanceof Jedis) {
            jedis = (Jedis) o;
        }
        // 验证连接是否有效
        jedis.get("test");

        return jedis;
    }

}