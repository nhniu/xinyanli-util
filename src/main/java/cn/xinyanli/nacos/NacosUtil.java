package cn.xinyanli.nacos;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * nacos工具类
 *
 * @author hewei
 * @date 2019/10/28 9:47
 **/
@Slf4j
public class NacosUtil {

    private static final Map<String, Properties> CONFIG = new HashMap<>();
    private static final String DEFAULT_GROUP = "DEFAULT_GROUP";


    /**
     * 加载nacos配置
     * 如果配置中有env会继续加载对应的环境配置
     *
     * @param url      nacos地址
     * @param commonId 不带环境的配置
     * @return
     */
    public static Properties loadEnv(String url, String commonId, String runEnv) {

        String[] split = commonId.split("[.]");
        Assert.isTrue(split.length == 2, "only-support-2");

        String key = getDataKey(commonId, DEFAULT_GROUP);
        String commonDataId = split[0] + "." + split[1];
        load(url, key, commonDataId, DEFAULT_GROUP);

        Assert.notEmpty(runEnv, "runEnv cannot be emtpy");
        String envDataId = split[0] + "-" + runEnv + "." + split[1];
        // 加载环境配置
        load(url, key, envDataId, DEFAULT_GROUP);

        Properties properties = CONFIG.get(key);
        Assert.notNull(properties, "{},{}|all-not-exist", commonDataId,envDataId);
        return properties;
    }

    @SneakyThrows
    private static void load(String url, String key, String dataId, String group) {

        Properties reqProp = new Properties();
        reqProp.put("serverAddr", url);
        ConfigService configService = NacosFactory.createConfigService(reqProp);
        configService.addListener(dataId, group, new Listener() {
            @Override
            public void receiveConfigInfo(String configInfo) {
                log.info("reloadNacos config|{}|{}|{}", url, dataId, group);

                updateProperties(key, configInfo, true);
            }

            @Override
            public Executor getExecutor() {
                return null;
            }
        });

        String content = configService.getConfig(dataId, group, 2000);
        log.info("first-loadNacos-config|{}|{}|{}|exist={}", url, dataId, group, content != null);
        updateProperties(key, content, false);
    }

    private static String getDataKey(String dataId, String group) {
        return String.format("%s_%s", dataId, group);
    }

    @SneakyThrows
    private static void updateProperties(String key, String content, boolean listener) {
        if (StrUtil.isEmpty(content)) {
            return;
        }
        Properties newProp = new Properties();
        try (StringReader reader = new StringReader(content)) {
            newProp.load(reader);
        } catch (IOException e) {
            if (listener) {
                log.error("parseContentToMap|{}", e);
            } else {
                // 如果是启动时出错 直接停止服务
                throw e;
            }
        }

        // 移除空格 ,key默认移除了空格
        newProp.forEach((k, v) -> {
            newProp.put(k, v.toString().trim());
        });

        // 监听修改
        if (listener) {
            listenerMap.forEach((k, v) -> {
                String newVal = newProp.getProperty(k);
                if (!CONFIG.get(key).getProperty(k).equals(newVal)) {
                    v.exe(newVal);
                }
            });
        }

        // 更新
        if (CONFIG.containsKey(key)) {
            CONFIG.get(key).putAll(newProp);
        } else {
            CONFIG.put(key, newProp);
        }
    }

    private static Map<String, Callback> listenerMap = new HashMap();

    /**
     * 添加配置修改监听
     *
     * @param key
     * @param callback
     */
    public static void addListener(String key, Callback callback) {
        listenerMap.put(key, callback);
    }

    public interface Callback {
        void exe(String val);
    }

}
