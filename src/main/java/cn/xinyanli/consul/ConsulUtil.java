package cn.xinyanli.consul;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.xinyanli.core.util.ObjUtil;
import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.agent.model.NewService;
import com.ecwid.consul.v1.health.HealthServicesRequest;
import com.ecwid.consul.v1.health.model.HealthService;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.util.List;

/**
 * consul工具类
 *
 * @author hewei
 * @date 2019/10/30 15:04
 **/
@Slf4j
public class ConsulUtil {

    private ConsulClient client;
    private String serviceId;

    Config config;
    public ConsulUtil(Config config){
        Assert.notNull(config);
        this.config = config;

        register();
    }

    @Data
    public static class Config {
        Boolean enable;
        String name;
        /**
         * 注册的客户端地址 优先使用环境变量SERVER_IP
         */
        String address;
        Integer port;
        String healthUrl;
        String serverHost;
        Integer serverPort;
    }

    /**
     * 服务注册
     */
    @SneakyThrows
    public void register() {
        String address = ObjUtil.getVal(config.address);
        String name = ObjUtil.getRequired(config.name,"name");
        Integer port = ObjUtil.getRequired(config.port,"port");
        String healthUrl = ObjUtil.getRequired(config.healthUrl,"healthUrl");

        // 优先获取系统环境变量，再获取nacos 再获取ip
        String serverIp = System.getenv("SERVER_IP");
        address = StrUtil.isNotEmpty(serverIp) ? serverIp : StrUtil.isNotEmpty(address) ? address : getIp();

        this.serviceId = name + "-" + address.replaceAll("[.]", "-") + "-" + port;

        NewService newService = new NewService();
        newService.setName(name);
        newService.setAddress(address);
        newService.setPort(port);
        newService.setId(serviceId);

        NewService.Check serviceCheck = new NewService.Check();
        String http = "http://" + newService.getAddress() + ":" + newService.getPort() + healthUrl;
        serviceCheck.setHttp(http);
        // 健康检查时间
        serviceCheck.setInterval("5s");
        newService.setCheck(serviceCheck);

        client = createClient();
        client.agentServiceRegister(newService);
        log.info("consul-register-ok health|{}", http);

        registerDestroy();
    }

    /**
     * 销毁
     */
    public void registerDestroy() {

        Runtime run = Runtime.getRuntime();//当前 Java 应用程序相关的运行时对象。
        //注册新的虚拟机来关闭钩子
        run.addShutdownHook(new Thread(() -> {
            this.client.agentServiceDeregister(serviceId,null);
            log.info("unregister|{}", serviceId);
        }));
        log.info("register-consul-destroy-hook|{}", config.name);
    }

    /**
     * 获取ip 会优先获取局域网ip
     *
     * @return
     */
    @SneakyThrows
    private String getIp() {
        return InetAddress.getLocalHost().getHostAddress();
    }


    /**
     * 创建consul连接
     *
     * @return
     */
    public ConsulClient createClient() {
        String host = config.serverHost;
        Integer serverPort = config.serverPort;

        Assert.notEmpty(host);
        Assert.notNull(serverPort);

        return new ConsulClient(host, serverPort);
    }


    /**
     * 服务发现
     *
     * @param serviceName
     * @return
     */
    public String getServiceUrl(String serviceName) {

        ConsulClient client = createClient();
        //根据服务名称获取服务
        Response<List<HealthService>> services = client.getHealthServices(serviceName, HealthServicesRequest.newBuilder().build());
        //无服务抛出异常
        Assert.notEmpty(services.getValue());

        //随机取一个服务
        HealthService randomService = services.getValue().get(RandomUtil.randomInt(services.getValue().size()));

        //拼接服务的URL
        String serviceUrl = String.format("http://%s:%d", randomService.getService().getAddress(), randomService.getService().getPort());
        log.info("getServiceUrl|{}", serviceUrl);

        return serviceUrl;
    }

}
