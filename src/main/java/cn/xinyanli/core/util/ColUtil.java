package cn.xinyanli.core.util;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ColUtil {

    public static List<Object> array2List(Object[] objs){
        if(objs == null){
            return null;
        }
        List<Object> list = new ArrayList<>(objs.length);
        for (Object obj : objs) {
            list.add(obj);
        }
        return list;
    }

    public static <V> BigDecimal sum(Collection<V> beans, Function<V, BigDecimal> prop) {
        BigDecimal sum = BigDecimal.ZERO;
        for (V bean : beans) {
            sum = sum.add(prop.apply(bean));
        }
        return sum;
    }

    public static <V> int sumInt(Collection<V> beans, Function<V, Integer> prop) {
        int sum = 0;
        for (V bean : beans) {
            sum = sum + (prop.apply(bean));
        }
        return sum;
    }


    /**
     * list转map
     */
    public static <K, V> Map<K, V> list2map(Collection<V> list1, Function<V, K> prop) {

        Map<K, V> map = new LinkedHashMap<>();
        for (V bean : list1) {
            map.put(prop.apply(bean), bean);
        }
        return map;
    }

    /**
     * 有重复的key ,list转map<key,list<v>>
     */
    public static <K, V>  Map<K, List<V>> list2mapList(Collection<V> list1, Function<V, K> prop) {

        Map<K, List<V>> map = new LinkedHashMap<>();
        for (V bean : list1) {
            K apply = prop.apply(bean);
            if(!map.containsKey(apply) ){
                map.put(apply,new ArrayList<>());
            }
            List<V> vs = map.get(apply);
            vs.add(bean);
        }
        return map;
    }

    /**
     * 获取bean集合的某个属性的集合
     * 相当于java8 pages.getContent().stream().map(v -> v.getSpuId()).collect(Collectors.toSet())
     *
     * @return
     */
    public static <K, V> List<K> list2Props(Collection<V> beans, Function<V, K> prop) {
        List<K> list = new ArrayList<>();
        for (V bean : beans) {
            list.add(prop.apply(bean));
        }

        return list;
    }

    /**
     * 获取bean集合的某个属性的集合
      *
     * @return
     */
    public static <K, V> List<K> list2Props(Collection<V> beans, Function<V, K> prop, Function<V, K> prop2) {
        Set<K> sets = new LinkedHashSet<>();
        for (V bean : beans) {
            sets.add(prop.apply(bean));
            sets.add(prop2.apply(bean));
        }

        return new ArrayList<>(sets);
    }

    /**
     * 将一个list的与另一个list 通过key对应起来
     * @param from 数据来源
     * @param fromKey  来源的key
     * @param to 复制到
     * @param toKey  对应的key 与来源的key对应
     * @param toSetMethod 赋值方法
     */
    public static <K, V, P> void listApply(Collection<V> from, Function<V, K> fromKey,
                                           Collection<P> to, Function<P, K> toKey, BiFunction<P, V, ?> toSetMethod) {
        Map<K, V> articleMap = ColUtil.list2map(from, fromKey);


        to.forEach(v -> toSetMethod.apply(v, articleMap.get(toKey.apply(v))));
    }


}
