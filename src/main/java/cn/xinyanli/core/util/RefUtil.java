package cn.xinyanli.core.util;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class RefUtil {



    /**
     * 如果不存在给默认值
     * @param c
     * @param fieldName
     * @param deft
     * @return
     */
    @SneakyThrows
    public static Object getStaticFieldVal(Class c,String fieldName,Object deft){
        Field tname2 = null;
        try {
            tname2 = c.getField(fieldName);
        } catch (NoSuchFieldException e) {
            return deft;
        }
        return tname2.get(c);
    }

    /**
     * 获取类静态属性
     *
     * @param c
     * @param fieldName
     * @return
     */
    @SneakyThrows
    public static Object getStaticFieldVal(Class c, String fieldName) {
        Field tname2 = null;
        try {
            tname2 = c.getField(fieldName);
        } catch (NoSuchFieldException e) {
            log.error("{} not statis string {}", c, fieldName);
            throw e;
        }
        return tname2.get(c);
    }
}
