package cn.xinyanli.core.util;

import cn.hutool.core.lang.Assert;

public class ObjUtil {
    public static <T> T getVal(T o) {
        return o;
    }

    public static <T> T getVal(T o, T deft) {
        return o == null ? deft : o;
    }

    public static <T> T getRequired(T o, String msg) {
        Assert.notEmpty("getRequired-msg cannot be emtpy");
        Assert.notNull(o, msg + " cannot be emtpy");

        return o;
    }
}
