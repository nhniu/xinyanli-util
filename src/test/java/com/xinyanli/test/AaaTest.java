package com.xinyanli.test;

import cn.xinyanli.db.JdbcUtil;
import cn.xinyanli.db.lambda.Lq;
import com.xinyanli.test.entity.TestOne;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AaaTest {

    /*


       CREATE TABLE `t_test_one` (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `type` varchar(255) DEFAULT NULL,
       `msg` varchar(255) DEFAULT NULL,
       `user_id` bigint(20) DEFAULT NULL,
       `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY (`id`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        &useServerPrepStmts=false&rewriteBatchedStatements=true

       批量插入效率对比
       10万条 addBatch方式 并且 &rewriteBatchedStatements=true   1秒  开不开事物效果一样
       不用rewriteBatchedStatements 不开事物 19秒   开启事物9秒
       直接生成sql插入14.7秒  ,使用事物6.5秒

        批量修改
        addBatch方式 19秒  ,开启事物11秒   ，开启事务并且 &rewriteBatchedStatements=true   6.6秒
        生成sql方式 19秒 ,开启事物 8.5秒

        总结
        addBatch缺点如果有些记录有值有些没值 sql不一样使用多个ps 会降低效率 需要加rewriteBatchedStatements=true
        addBatch 插入或者修改的sql语句固定 只是值不一样

        直接生成sql需要加allowMultiQueries=true允许执行多条语句

     */


    @Test
    public void testDb() {

        JdbcUtil jdbcUtil = new JdbcUtil.Builder()
                .setUrl("jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf-8" +
                        "    &useServerPrepStmts=false&rewriteBatchedStatements=true &allowMultiQueries=true&serverTimezone=Asia/Shanghai")
                .setSiginConn(true)
                .setUsername("root").setPassword("root").setPrintSql(true).build();
        long l = System.currentTimeMillis();
        testSql(jdbcUtil);
        long l2 = System.currentTimeMillis();
        System.out.println((l2 - l) / 1000f);
    }

    private void testSql(JdbcUtil jdbcUtil){
        List<Lq> lqs = new ArrayList();
        Lq lq = Lq.n().set(TestOne::getMsg, "asdfsdf4444").eq(TestOne::getId, 1);
        lqs.add(lq);
        lqs.add(lq);
        jdbcUtil.tx(() -> {

            int[] update = jdbcUtil.update(lqs);

            System.out.println(Arrays.toString(update));
            return true;
        });
    }

    private void batch1(JdbcUtil jdbcUtil) {
        List<TestOne> ones = new ArrayList<>();
        for (long i = 0; i < 10_0000; i++) {
            TestOne one = new TestOne().setMsg("asdf").setType("xxxxxx").setUserId(i);

            ones.add(one);
        }
        // 初始化 减少反射时间影响
        jdbcUtil.insert(new TestOne().setMsg("asdf").setType("xxxxxx").setUserId(000L));
        long l = System.currentTimeMillis();
        jdbcUtil.tx(() -> {

            jdbcUtil.insert(ones);

            return true;
        });
        long l2 = System.currentTimeMillis();
        System.out.println((l2 - l) / 1000f);
    }


    private void batch2(JdbcUtil jdbcUtil) {

        int[] n = jdbcUtil.batchExecute("insert into t_test_one(msg,type,user_id) values(q?)", p -> {

                    for (long i = 0; i < 10_0000; i++) {
                        jdbcUtil.batchAdd(p, "hjgjhgk", "xxxxxxx", i);
                    }
                }
        );
    }


    private void batch3(JdbcUtil jdbcUtil) {
        jdbcUtil.tx(() -> {
            jdbcUtil.batchExecute("update t_test_one set msg = ?,type = ? where id = ?", p -> {

                for (long i = 0; i < 10_0000; i++) {
                    jdbcUtil.batchAdd(p, "hjgjhgk", "xxxxxxx", i);
                }


            });
            return true;
        });

    }


    private void batch4(JdbcUtil jdbcUtil) {
        List<TestOne> ones = new ArrayList<>();
        for (long i = 0; i < 10_0000; i++) {
            TestOne one = new TestOne().setMsg("xxxasdfasdfsdf").setType("xxxxxx").setUserId(i).setId(i);
            if(i % 5 == 0){
                one.setMsg(null);
            }
            ones.add(one);
        }

        jdbcUtil.tx(() -> {
            int[] i = jdbcUtil.updateById(ones);
             return true;
        });

    }
}
