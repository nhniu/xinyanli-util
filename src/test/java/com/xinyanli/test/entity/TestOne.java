package com.xinyanli.test.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TestOne {

    public static final String TNAME = "t_test_one";

    Long id;

    String type;
    String msg;
    Long userId;
}
